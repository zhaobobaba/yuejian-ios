//
//  ROFoodDetailTableViewController.swift
//  LuYao
//
//  Created by luyao on 2017/4/22.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class ROFoodDetailTableViewController: UITableViewController {
    var food : ROFoodModel!
    //要想在viewdidload加载评论先要设定一个数组存放加载的评论
    var commentList:NSArray = NSArray()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        //显示名称
        self.title = food.foodname as String
        //
        NotificationCenter.default.addObserver(self, selector:#selector(ROFoodDetailTableViewController.makeOrder),name:NSNotification.Name(rawValue: "Order"), object: nil)
        //加载评论的评论的时候
        let paraDict = ["food_id":food.food_id!] as NSDictionary
   RONetworkMngTool.sharedNetWorkMngTool().RONetwork_GetFoodCommentListParameters(paraDict, view: self.view){ (commentArray) in
    //把返回评论的数组放在我们的属性里面
            self.commentList = commentArray
            self.tableView.reloadData()
        }
    }
   //
    func makeOrder(){
        self.performSegue(withIdentifier: "toOrder", sender: food)
        
    }
    //传旨的时候进行跳转
    //跳转的目标就是order 讲我传过来的send赋值给下一个对象
    override func prepare(for segue:UIStoryboardSegue, sender: Any?) {
        let orderController: ROOrderTableViewController = segue.destination as!  ROOrderTableViewController
        //构建传递food模型和shop_id的dictionary
        orderController.food = sender as! ROFoodModel
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            
            return 3
        }
        else {
            return self.commentList.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var basecell: UITableViewCell = UITableViewCell()
        //机遇tableview cell的对象
        if indexPath.section == 0 {
        if (indexPath as NSIndexPath).row == 0 {
            //在创建个性化的对象
            let cell : ROFoodHeaderCell = tableView.dequeueReusableCell(withIdentifier: "foodHeaderCell", for: indexPath) as! ROFoodHeaderCell
            cell.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: CELLHEIGHT);
            
            tableView.separatorStyle = UITableViewCellSeparatorStyle.none;
            
            let imageStr = food.pic
            let imageURL = URL(string: imageStr as! String)
            cell.foodImageView!.setImageWith(imageURL!, placeholderImage: UIImage(named: "placeHolder"))
            
            //菜品名称
            cell.foodName.text = "\(food.foodname!)"
            //菜品价格
            cell.price.text = "\(food.price!)元"
            //推荐指数
            cell.recommand.text = "\(food.recommand!)"
            //商店编号
            cell.shop_id.text = "\(food.shop_id!)"
            basecell = cell
            
        } else if((indexPath as NSIndexPath).row == 1){
            let cell: ROFoodIntroduceCell = tableView.dequeueReusableCell(withIdentifier: "foodIntrCell", for: indexPath) as! ROFoodIntroduceCell
            //可重用的查找表示
            //先设置模型
            cell.food = food;
            //再算Cell的frame
            cell.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: ROFoodIntroduceCell.cellHeightWithPostinModel(food));
            basecell = cell;
        }
        else if((indexPath as NSIndexPath).row == 2) {
                                      //调用他的创建tableview的方法
            let cell = foodDetailCell.creatCell(tableView)
            //food传给fooddetailcell的food
            cell.food = food
            basecell = cell
        }
            
    }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "foodCommentCell", for: indexPath) as! FoodCommentCell
            cell.selectionStyle = .none
            if commentList.count == 0 {
                cell.textLabel?.text = "无评价"
            }else{
                let dict = commentList[indexPath.row] as! ROCommentModel
                cell.textLabel?.text = dict.content as String
                cell.foodName.text = (dict.foodname as String)
                cell.price.text = "单价:\(dict.price as Int)元"
                cell.shopName.text = dict.shopname as String
                cell.userName.text = "\(dict.username as String)--\(dict.comment_time as String)"
            }
            basecell = cell
        }
        return basecell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if ((indexPath as NSIndexPath).row == 0) {
            return CELLHEIGHT;
        }else if((indexPath as NSIndexPath).row == 1){
            //返回可变的cell高度 需要根据模型的内容进行计算
            // return ROCourseIntroduceCell.cellHeightWithPostinModel(food)
            return CELLHEIGHT;
            
        }else{
            return 60;
        }
        
        
    }
    //设定分区的视图方法
    //设定一个headerview的一个高度
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1{
            return 40
        }else {
            return 0
        }
    }
    //返回headerview的一个视图
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
            let headerView = UIView()
            //在视图中设定一个UIview的子类
            headerView.frame = CGRect(x: 0,y: 0, width: self.view.frame.width,height: 40)
            headerView.backgroundColor = UIColor.darkGray
            let titleLabel = UILabel()
            titleLabel.text = "菜品评论" as String
            titleLabel.textColor = UIColor.cyan
            titleLabel.font = UIFont.systemFont(ofSize: 20)
            titleLabel.sizeToFit()
            titleLabel.center = CGPoint(x: self.view.frame.width/2, y:20)
            headerView.addSubview(titleLabel)
            headerView.addSubview(titleLabel)
            return headerView
        }else {
            return nil
        }
    }
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
}




