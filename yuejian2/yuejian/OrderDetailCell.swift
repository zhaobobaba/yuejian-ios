//
//  OrderDetailCell.swift
//  LuYao
//
//  Created by luyao on 2017/5/6.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class OrderDetailCell: UITableViewCell {

    var food_id: NSNumber!
    var food: ROFoodModel! {
        didSet {
            food_id = food.food_id
        }
    }
    var foodImageView: UIImageView!
    //定义菜品名字title
    var foodNameTitle: UILabel!
    //定义菜品名字
    var foodName: UILabel!
    //定义价钱title
    var priceTitle: UILabel!
    //定义价钱
    var price: UILabel!
    //定义推荐指数title
    var recommandTitle: UILabel!
    //定义推荐指数
    var recommand: UILabel!
    
    //定义商店编号title
    var shop_idTitle: UILabel!
    //定义商店编号
    var shop_id: UILabel!
    //购买数量
    var numTitle: UILabel!
    var num: UITextField!
    //总价
    var totalPriceTitle: UILabel!
    var totalPrice: UILabel!
    //当前时间
    var currentTime:String!
    //建议配送时间
    var suggestTime:String!
    //下单时间
    var orderTimeLabel: UILabel!
    //配送时间
    var deliverTimeLabel: UILabel!
    
    //确认btn
    var commitBtn: UIButton!
    //取消btn
    var cancelBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        foodImageView = UIImageView()
        self.contentView.addSubview(foodImageView)
        
        foodNameTitle = UILabel()
        foodNameTitle.text = "菜品名称"
        foodNameTitle.font = UIFont.systemFont(ofSize: 16)
        self.contentView.addSubview(foodNameTitle)
        
        foodName = UILabel()
        self.contentView.addSubview(foodName)
        
        priceTitle = UILabel()
        priceTitle.text = "菜品单价"
        priceTitle.font = UIFont.systemFont(ofSize: 16)
        self.contentView.addSubview(priceTitle)
        
        price = UILabel()
        self.contentView.addSubview(price)
        
        recommandTitle = UILabel()
        recommandTitle.text = "推荐指数"
        recommandTitle.font = UIFont.systemFont(ofSize: 16)
        self.contentView.addSubview(recommandTitle)
        
        recommand = UILabel()
        self.contentView.addSubview(recommand)
        
        shop_idTitle = UILabel()
        shop_idTitle.text = "店铺编号"
        shop_idTitle.font = UIFont.systemFont(ofSize: 16)
        self.contentView.addSubview(shop_idTitle)
        
        shop_id = UILabel()
        self.contentView.addSubview(shop_id)
        
        numTitle = UILabel()
        numTitle.text = "购买数量"
        numTitle.font = UIFont.systemFont(ofSize: 16)
        self.contentView.addSubview(numTitle)
        
        num = UITextField()
        // num.delegate = self
        num.backgroundColor = UIColor.yellow
        num.font = UIFont.systemFont(ofSize: 16)
        num.text = "1"
        num.keyboardType = .decimalPad
        NotificationCenter.default.addObserver(self, selector: #selector(textfieldDidChange), name: .UITextFieldTextDidChange, object: num)
        
        self.contentView.addSubview(num)
        
        totalPriceTitle = UILabel()
        totalPriceTitle.text = "总价"
        totalPriceTitle.font = UIFont.systemFont(ofSize: 16)
        self.contentView.addSubview(totalPriceTitle)
        
        totalPrice = UILabel()
        totalPrice.font = UIFont.systemFont(ofSize: 16)
        totalPrice.text = "23"
        self.contentView.addSubview(totalPrice)
        
        orderTimeLabel = UILabel()
        orderTimeLabel.font = UIFont.systemFont(ofSize: 16)
        self.contentView.addSubview(orderTimeLabel)
        
        deliverTimeLabel = UILabel()
        deliverTimeLabel.font = UIFont.systemFont(ofSize: 16)
        self.contentView.addSubview(deliverTimeLabel)
        
        let nowDate = NSDate()
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        currentTime = formatter.string(from: nowDate as Date)
        var timeInterval:TimeInterval = nowDate.timeIntervalSince1970
        let timeStamp = Int(timeInterval) + 1800
        timeInterval = TimeInterval(timeStamp)
        let date = NSDate(timeIntervalSince1970: timeInterval)
        suggestTime = formatter.string(from: date as Date)
        orderTimeLabel.text = "下单时间:\(currentTime!)"
        deliverTimeLabel.text = "送货时间:\(currentTime!)-\(suggestTime!)"

        
        commitBtn = UIButton()
        commitBtn.backgroundColor = UIColor.yellow
        commitBtn.setTitleColor(UIColor.black, for: UIControlState.normal)
        commitBtn.setTitle("确定", for: UIControlState.normal)
        commitBtn.addTarget(self, action:#selector(commit) , for: UIControlEvents.touchUpInside)
        self.contentView.addSubview(commitBtn)
        
        cancelBtn = UIButton()
        cancelBtn.backgroundColor = UIColor.yellow
        cancelBtn.setTitleColor(UIColor.black, for: UIControlState.normal)
        cancelBtn.setTitle("取消", for: UIControlState.normal)
        cancelBtn.addTarget(self, action:#selector(cancel) , for: UIControlEvents.touchUpInside)
        self.contentView.addSubview(cancelBtn)
    }
    //
    func textfieldDidChange(){
        if num.text?.characters.count != 0 {
            let total = Float(num.text!)!*Float(price.text!)!
            totalPrice.text = "\(total)"
        }else{
            totalPrice.text = "0"
        }
    }
    
    func commit() {
        let parameters:NSDictionary = ["user_id":userId.intValue,"food_id":food_id,"num":Int(num.text!)!,"sum":Double(totalPrice.text!)!,"suggesttime":"\(currentTime)-\(suggestTime)"]
        RONetworkMngTool.sharedNetWorkMngTool().RONetwork_insertOrderWithParameters(parameters, view: UIView(), block: { (flag) -> Void in
            if flag == "1" {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Commit"), object: nil)
            }
        })
    }
    
    func cancel() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Commit"), object: nil)
    }
    //2.设置控件位置
    override func layoutSubviews() {
        super.layoutSubviews()
        foodImageView.frame = CGRect(x: 0, y: 0, width: CGFloat(WHRatio * CELLHEIGHT), height: CELLHEIGHT);
        
        foodNameTitle.frame = CGRect(x: WHRatio * CELLHEIGHT + MARGIN, y: 0, width: 80, height: 20);
        
        foodName.frame = CGRect( x: WHRatio * CELLHEIGHT + MARGIN + 80, y: 0,width: ScreenWidth-(WHRatio*CELLHEIGHT + MARGIN + 80), height: 20);
        
        priceTitle.frame = CGRect(x: WHRatio * CELLHEIGHT + MARGIN, y: 20, width: 80, height: 20);
        
        price.frame = CGRect(x: WHRatio * CELLHEIGHT + MARGIN + 80, y: 20,width: ScreenWidth-(WHRatio * CELLHEIGHT + MARGIN + 80), height: 20);
        
        recommandTitle.frame = CGRect(x: WHRatio * CELLHEIGHT + MARGIN, y: 40, width: 80, height: 20);
        
        recommand.frame = CGRect(x: WHRatio * CELLHEIGHT + MARGIN + 80, y: 40, width: ScreenWidth-(WHRatio * CELLHEIGHT + MARGIN + 80), height: 20);
        
        shop_idTitle.frame = CGRect(x: WHRatio * CELLHEIGHT + MARGIN, y: 60, width: 80, height: 20);
        
        shop_id.frame = CGRect(x: WHRatio * CELLHEIGHT + MARGIN + 80, y: 60, width: ScreenWidth-(WHRatio * CELLHEIGHT + MARGIN + 80), height: 20);
        
        numTitle.frame = CGRect(x: WHRatio * CELLHEIGHT + MARGIN, y: 80, width: 80, height: 20);
        num.frame = CGRect(x: WHRatio * CELLHEIGHT + MARGIN + 80, y: 80, width: 120, height: 20);
        
        totalPriceTitle.frame = CGRect(x: WHRatio * CELLHEIGHT + MARGIN, y: 100, width: 80, height: 20);
        totalPrice.frame = CGRect(x: WHRatio * CELLHEIGHT + MARGIN + 80, y: 100, width: 120, height: 20);
        
        orderTimeLabel.frame = CGRect(x: WHRatio * CELLHEIGHT + MARGIN, y: 120, width: 200, height: 20);
        
        deliverTimeLabel.frame = CGRect(x: WHRatio * CELLHEIGHT + MARGIN, y: 140, width: 200, height: 20);
        
        commitBtn.frame = CGRect(x: MARGIN + 50, y: 200, width: 90, height: 30);
        
        cancelBtn.frame = CGRect(x:  MARGIN + 200, y: 200, width: 90, height: 30);
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
