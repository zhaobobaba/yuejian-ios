//
//  foodDetailCell.swift
//  LuYao
//
//  Created by luyao on 2017/5/6.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class foodDetailCell: UITableViewCell {

    
    @IBOutlet weak var telNum: UILabel!
    //定义了一个电话号码 label里面的电话号码需要通过接口实现
    var phonenum: NSString!{//一旦这个phone被赋值了 我们就要给label赋值
        willSet(newValue) {
            telNum.text = newValue as String;
        }//设立了一个观察者模式当这个phone有值的时候给当前的labeltext值
        
        
    }
    
    //一旦它被controller里面的food赋值了
    var food : ROFoodModel! {
        didSet(oldValue){
            //判断他之前是否被赋值过 collect方法在
            self.isCollect()
            let parameters: NSDictionary = ["food_id":food.food_id]
            //电话号码仍然通过一个接口来实现
            //根据foodID来获取电话号码
            RONetworkMngTool.sharedNetWorkMngTool().RONetwork_getShopInformationWithParameters(parameters, view:UIView()) { (phonenum) in//从 tool返回给这个参数
                //把返回给当前的phonenum
                self.phonenum = phonenum
            }
        }
    }
    
    
    @IBOutlet weak var collectionBtn: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    @IBAction func makeCall(_ sender: UIButton) {
        //真机测试
        let url = NSURL(string: "tel://" + (phonenum as String
            ))
        UIApplication.shared.open(url! as URL, options:[:] , completionHandler: nil)
    }
    //下订单
    @IBAction func order(_ sender: UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Order"), object: nil)
    }
    //
    @IBAction func collection(_ sender: UIButton) {
        let parameters1: NSDictionary = ["user_id": userId,"food_id": self.food.food_id] as NSDictionary
        RONetworkMngTool.sharedNetWorkMngTool().RONetwork_collectFoodWithParameters(parameters1, view: UIView.init()) { (success) in
            if success == "1" {
                self.isCollect()
            } else {
                print("收藏/取消收藏失败！")
            }
        }
    }
    //判断是否收藏 这个方法写在接口里面
    func isCollect() {
        let parameters2: NSDictionary = ["user_id": userId,"shop_food_id": self.food.food_id,"flag": 1] as NSDictionary
        //flag等于1判断的是菜谱 用户IDfoodID
        RONetworkMngTool.sharedNetWorkMngTool().RONetwork_isCollectedWithParameters(parameters2, view: UIView.init()) { (isCollect) in
            if isCollect == "0" {
                self.collectionBtn.setTitle("收藏", for: UIControlState.normal)
                //if0没有收藏当前title表示收藏
            }else if isCollect == "1" {
                self.collectionBtn.setTitle("取消收藏", for: UIControlState.normal)
            }
        }
    }
    
    class  func  creatCell(_ tableView : UITableView) -> foodDetailCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "foodDetailCell")
        if  cell == nil {
            cell = (Bundle.main.loadNibNamed("foodDetailCell", owner: nil, options: nil))?.first as! foodDetailCell
        }
        return cell as! foodDetailCell;
    }
    
    
}
