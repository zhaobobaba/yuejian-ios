//
//  ROShopListTableViewController.swift
//  LuYao
//
//  Created by luyao on 2017/4/22.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit
let  CELLHEIGHT: CGFloat = 100.0
let  MARGIN: CGFloat = 8.0
let ScreenWidth: CGFloat = UIScreen.main.bounds.width
let ScreenHeight: CGFloat = UIScreen.main.bounds.height
let WHRatio: CGFloat = 133/100.0

class ROFoodHeaderCell: UITableViewCell {
    //菜品图片
    var foodImageView: UIImageView!
    //定义菜品名字title
    var foodNameTitle: UILabel!
    //定义菜品名字
    var foodName: UILabel!
    //定义价钱title
    var priceTitle: UILabel!
    //定义价钱
    var price: UILabel!
    //定义推荐指数title
    var recommandTitle: UILabel!
    //定义推荐指数
    var recommand: UILabel!
    
    //定义商店编号title
    var shop_idTitle: UILabel!
    //定义商店编号
    var shop_id: UILabel!
    
    //1.在以下函数添加子控件
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        foodImageView = UIImageView()
        self.contentView.addSubview(foodImageView)
        
        foodNameTitle = UILabel()
        foodNameTitle.text = "菜品名称"
        foodNameTitle.font = UIFont.systemFont(ofSize: 16)
        self.contentView.addSubview(foodNameTitle)
        
        foodName = UILabel()
        self.contentView.addSubview(foodName)
        
        priceTitle = UILabel()
        priceTitle.text = "菜品单价"
        priceTitle.font = UIFont.systemFont(ofSize: 16)
        self.contentView.addSubview(priceTitle)
        
        price = UILabel()
        self.contentView.addSubview(price)
        
        recommandTitle = UILabel()
        recommandTitle.text = "推荐指数"
        recommandTitle.font = UIFont.systemFont(ofSize: 16)
        self.contentView.addSubview(recommandTitle)
        
        recommand = UILabel()
        self.contentView.addSubview(recommand)
        
        shop_idTitle = UILabel()
        shop_idTitle.text = "店铺编号"
        shop_idTitle.font = UIFont.systemFont(ofSize: 16)
        self.contentView.addSubview(shop_idTitle)
        
        shop_id = UILabel()
        self.contentView.addSubview(shop_id)
        
    }
    //2.设置控件位置
    override func layoutSubviews() {
        super.layoutSubviews()
        foodImageView.frame = CGRect(x: 0, y: 0, width: CGFloat(WHRatio * CELLHEIGHT), height: CELLHEIGHT);
        
        foodNameTitle.frame = CGRect(x: WHRatio * CELLHEIGHT + MARGIN, y: 0, width: 80, height: 20);
        
        foodName.frame = CGRect( x: WHRatio * CELLHEIGHT + MARGIN + 80, y: 0,width: ScreenWidth-(WHRatio*CELLHEIGHT + MARGIN + 80), height: 20);
        
        priceTitle.frame = CGRect(x: WHRatio * CELLHEIGHT + MARGIN, y: 20, width: 80, height: 20);
        
        price.frame = CGRect(x: WHRatio * CELLHEIGHT + MARGIN + 80, y: 20,width: ScreenWidth-(WHRatio * CELLHEIGHT + MARGIN + 80), height: 20);
        
        
        recommandTitle.frame = CGRect(x: WHRatio * CELLHEIGHT + MARGIN, y: 40, width: 80, height: 20);
        
        recommand.frame = CGRect(x: WHRatio * CELLHEIGHT + MARGIN + 80, y: 40, width: ScreenWidth-(WHRatio * CELLHEIGHT + MARGIN + 80), height: 20);
        
        shop_idTitle.frame = CGRect(x: WHRatio * CELLHEIGHT + MARGIN, y: 60, width: 80, height: 20);
        
        shop_id.frame = CGRect(x: WHRatio * CELLHEIGHT + MARGIN + 80, y: 60, width: ScreenWidth-(WHRatio * CELLHEIGHT + MARGIN + 80), height: 20);
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
