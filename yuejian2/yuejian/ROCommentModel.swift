//
//  ROCommentModel.swift
//  LuYao
//
//  Created by luyao on 2017/5/24.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit
class ROCommentModel: NSObject {var comment_time: NSString!
var content: NSString!
var food_id: NSNumber!
var foodname: NSString!
var num: NSNumber!
var order_id: NSNumber!
var ordertime: NSString!
var price: NSNumber!
var shopaddress: NSString!
var shopname: NSString!
var suggesttime: NSString!
var sum: NSNumber!
var user_id: NSNumber!
var username:NSString!


class func commentModelWithDict(_ dict: NSDictionary) -> ROCommentModel {
    let model = ROCommentModel()
    //根据字典，进行模型的初始化
    model.setValuesForKeys(dict as! [String: AnyObject])
    return model
    
}


}
