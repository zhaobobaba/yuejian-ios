//
//  ROOrderTableViewController.swift
//  LuYao
//
//  Created by luyao on 2017/5/6.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class ROOrderTableViewController: UITableViewController {
    var food : ROFoodModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = food.foodname as String
        //观察者
        NotificationCenter.default.addObserver(self, selector:#selector(ROOrderTableViewController.commit),name:NSNotification.Name(rawValue: "Commit"), object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var basecell: UITableViewCell = UITableViewCell()
        
        if (indexPath as NSIndexPath).row == 0 {
            let cell : OrderDetailCell = tableView.dequeueReusableCell(withIdentifier: "orderDetail", for: indexPath) as! OrderDetailCell
            cell.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: CELLHEIGHT);
            
            tableView.separatorStyle = UITableViewCellSeparatorStyle.none;
            
            let imageStr = food.pic
            let imageURL = URL(string: imageStr as! String)
            cell.foodImageView!.setImageWith(imageURL!, placeholderImage: UIImage(named: "placeHolder"))
            cell.food = self.food
            //菜品名称
            cell.foodName.text = "\(food.foodname!)"
            //菜品价格
            cell.price.text = "\(food.price!)"
            //推荐指数
            cell.recommand.text = "\(food.recommand!)"
            //商店编号
            cell.shop_id.text = "\(food.shop_id!)"
            basecell = cell
        }
               return basecell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      
        return 300;
       
    }
    //
    func commit() {
        let _ = self.navigationController?.popViewController(animated: true)
    }

   
}
