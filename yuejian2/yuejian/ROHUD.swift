//
//  ROHUD.swift
//  yuejian
//
//  Created by yuejian on 2017/9/29.
//  Copyright © 2017年 yuejian. All rights reserved.
//

import Foundation
//做MBProgressHUD的扩展
extension MBProgressHUD {
    class func showDelayHUDToView(_ view: UIView, message: String) ->Void {
        let HUD = MBProgressHUD(view:view)
        view.addSubview(HUD!)
        HUD?.yOffset = Float(view.frame.size.height * 1.0/4.0)
        //不显示转圈，只显示一句提示的话
        HUD?.customView = UIImageView(frame: CGRect.zero)
        HUD?.mode = MBProgressHUDMode.customView
        HUD?.labelText = message
        HUD?.show(true)
        HUD?.hide(true, afterDelay: 1)
    }
}
