//
//  ROShopModel.swift
//  LuYao
//
//  Created by luyao on 2017/4/22.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class ROShopModel: NSObject {
    var address: NSString!
    var comment: NSString!
    var intro: NSString!
    var level: NSNumber!
    var phonenum: NSString!
    var pic: NSString!
    var shop_id: NSNumber!
    var shopname: NSString!
    
    class func shopModelWithDict(_ dict: NSDictionary) -> ROShopModel {
        let model = ROShopModel()
        //根据字典，进行模型初始化
        model.setValuesForKeys(dict as! [String: AnyObject])
        return model
    }
    
}
