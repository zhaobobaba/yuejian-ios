//
//  ROCollectionViewController.swift
//  LuYao
//
//  Created by luyao on 2017/5/27.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class ROCollectionViewController: UIViewController,UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource{
        
        var scrollView:UIScrollView!
        
        var shopBtn:UIButton!
        var shopTable = UITableView()
        
        var foodBtn:UIButton!
        var foodTable = UITableView()
        
        var shapeLayer:CAShapeLayer!
        
        let WIDTH = UIScreen.main.bounds.width
        let HEIGHT = UIScreen.main.bounds.height
        
        var shopListArray:NSArray!
        var foodListArray:NSArray!
        
        override func viewDidLoad() {
            super.viewDidLoad()
            setView()
            getShopList()
            getFoodList()
            
        }
        //获取店铺
        func getShopList(){
            
            let parameters:NSDictionary = ["user_id":userId,"flag":0]
            RONetworkMngTool.sharedNetWorkMngTool().RONetwork_GetCollectionListParameters(parameters, view: self.view) { (collectionShopList) in
                self.shopListArray = collectionShopList as NSArray
                self.shopTable.reloadData()
            }
        }
        //获取菜谱
        func getFoodList(){
            let parameters:NSDictionary = ["user_id":userId,"flag":1]
            RONetworkMngTool.sharedNetWorkMngTool().RONetwork_GetCollectionListParameters(parameters, view: self.view) { (collectionShopList) in
                self.foodListArray = collectionShopList as NSArray
                self.foodTable.reloadData()
            }
        }
        override func viewWillAppear(_ animated: Bool) {
            self.navigationController?.isNavigationBarHidden = true
        }
        
        func setView(){
            view.backgroundColor = UIColor.white
            self.automaticallyAdjustsScrollViewInsets = false
            scrollView = UIScrollView(frame: CGRect(x: 0, y: 60, width: WIDTH, height: HEIGHT-109))
            scrollView.backgroundColor = UIColor.red
            scrollView.showsVerticalScrollIndicator = false
            scrollView.showsHorizontalScrollIndicator = false
            scrollView.contentSize = CGSize(width: scrollView.bounds.width*2, height: HEIGHT-109)
            scrollView.isPagingEnabled = true
            scrollView.delegate = self
            view.addSubview(scrollView)
            
            for table in [shopTable,foodTable]{
                table.frame.size = scrollView.frame.size
                table.delegate = self
                table.dataSource = self
                scrollView.addSubview(table)
            }
            shopTable.frame.origin = CGPoint.zero
            shopTable.register(CollectionCell.self, forCellReuseIdentifier: "shopTableCell")
            foodTable.frame.origin = CGPoint(x: WIDTH, y: 0)
            foodTable.register(CollectionCell.self, forCellReuseIdentifier: "foodTableCell")
            
            shopBtn = UIButton(frame: CGRect(x: 0, y: 20, width: WIDTH/2, height: 40))
            shopBtn.setTitle("店铺", for: .normal)
            shopBtn.setTitleColor(UIColor.black, for: .normal)
            shopBtn.backgroundColor = UIColor.gray
            shopBtn.addTarget(self, action: #selector(showShop), for: .touchUpInside)
            view.addSubview(shopBtn)
            
            foodBtn = UIButton(frame: CGRect(x: WIDTH/2, y: 20, width: view.bounds.width/2, height: 40))
            foodBtn.setTitle("菜谱", for: .normal)
            foodBtn.setTitleColor(UIColor.black, for: .normal)
            foodBtn.backgroundColor = UIColor.gray
            foodBtn.addTarget(self, action: #selector(showFood), for: .touchUpInside)
            view.addSubview(foodBtn)
            
            shapeLayer = CAShapeLayer()
            shapeLayer.path = UIBezierPath(roundedRect: CGRect(x:0, y:20, width:view.bounds.width/2, height:40), cornerRadius: 0).cgPath
            shapeLayer.fillColor = UIColor.yellow.cgColor
            shapeLayer.opacity = 0.6
            view.layer.addSublayer(shapeLayer)
        }
        
        func showShop(){
            scrollView.setContentOffset(CGPoint(x:0, y:0), animated: true)
            shapeLayer.position.x = 0
        }
        
        func showFood(){
            scrollView.setContentOffset(CGPoint(x:WIDTH, y:0), animated: true)
            shapeLayer.position.x = WIDTH/2
        }
        
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if tableView == shopTable && shopListArray != nil{
                return shopListArray.count
            }
            else if tableView == foodTable && foodListArray != nil{
                return foodListArray.count
            }
            return 1
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            var basicCell:UITableViewCell? = nil
            if tableView == shopTable {
                let cell = tableView.dequeueReusableCell(withIdentifier: "shopTableCell", for: indexPath)
                if shopListArray != nil{
                    let model = self.shopListArray[(indexPath as NSIndexPath).row] as! ROCollectionModel
                    let imageStr = model.pic as String
                    let imageUrl = URL(string: imageStr)
                    cell.imageView!.setImageWith(imageUrl, placeholderImage: UIImage(named: "placeHolder"))
                    cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
                    cell.textLabel?.text = model.shopname as String
                }
                basicCell = cell
            }
            if tableView == foodTable {
                let cell = tableView.dequeueReusableCell(withIdentifier: "foodTableCell", for: indexPath)
                if foodListArray != nil{
                    let model = self.foodListArray[(indexPath as NSIndexPath).row] as! ROCollectionModel
                    let imageStr = model.pic as String
                    let imageUrl = URL(string: imageStr)
                    cell.imageView!.setImageWith(imageUrl, placeholderImage: UIImage(named: "placeHolder"))
                    cell.textLabel?.text = model.foodname as String
                    
                    cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
                    
                }
                basicCell = cell
            }
            return basicCell!
        }
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            if tableView == shopTable{
                let currentShop: ROCollectionModel = shopListArray[(indexPath as NSIndexPath).row] as! ROCollectionModel
                let dic :NSDictionary = ["shop_id":currentShop.shop_id,"shop_name": currentShop.shopname]
                self.performSegue(withIdentifier: "toFoodList", sender: dic)
            }else if tableView == foodTable{
                
                let currentFood: ROCollectionModel = foodListArray[(indexPath as NSIndexPath).row] as! ROCollectionModel
                let food_id = currentFood.food_id!
                let parameters = ["food_id":food_id] as NSDictionary
                RONetworkMngTool.sharedNetWorkMngTool().RONetwork_getFoodInformationWithParameters(parameters, view: self.view, block: { (afood) in
                    self.performSegue(withIdentifier: "toFoodDetail2", sender:afood)
                })
                
            }
        }
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            if segue.identifier == "toFoodList" {
                
                let foodListController: ROFoodListTableViewController = segue.destination as! ROFoodListTableViewController
                
                foodListController.shopId = (sender as! NSDictionary).object(forKey: "shop_id") as! NSNumber!
                foodListController.shopName = (sender as! NSDictionary).object(forKey: "shop_name") as! NSString!
            }else if segue.identifier == "toFoodDetail2"{
                
                let foodDetailController: ROFoodDetailTableViewController = segue.destination as! ROFoodDetailTableViewController
                foodDetailController.food = sender as! ROFoodModel
                
            }
        }
        func scrollViewDidScroll(_ scrollView: UIScrollView) {
            let offsetX = scrollView.contentOffset.x
            shapeLayer.position.x = offsetX/2
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return  80
        }
        
}
