//
//  ROShopListTableViewController.swift
//  LuYao
//
//  Created by luyao on 2017/4/22.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class ROFoodIntroduceCell: UITableViewCell {

    //菜品名称属性
    var  title: UILabel!
    //菜品介绍
    var  foodIntroduce: UILabel!
    
    var food: ROFoodModel! {
        didSet(newValue) {
            //print(food.intro) 显示介绍
            foodIntroduce.text = food.intro as String;
        }
    }
    // var food: ROFoodModel!
    //添加子控件
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //标题
        title = UILabel()
        title.text = "菜品介绍"
        title.font = UIFont.systemFont(ofSize: 18)
        title.textColor = UIColor.blue
        self.contentView.addSubview(title)
        
        //课程介绍正文
        foodIntroduce = UILabel()
        foodIntroduce.font = UIFont.systemFont(ofSize: 14)
        foodIntroduce.textColor = UIColor.gray
        //显示多行
        foodIntroduce.numberOfLines = 0
        self.contentView.addSubview(foodIntroduce)
        
        
        
        
    }
    //设置位置， 这里由于不知道正文有多少字，所以正文的位置确定的前提是需要知道正文是什么，当cell的frame值一发生变化就是触发这个方法
    override func layoutSubviews() {
        super.layoutSubviews()
        title.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width - 16,height: 20)
        title.textAlignment = NSTextAlignment.center
        
        let cix: CGFloat = 8
        let ciy: CGFloat = 28
        let ciw: CGFloat = UIScreen.main.bounds.size.width - 16
        var cih: CGFloat = 0
        //需要根据正文来计算
        if food != nil {
            //得到正文
            let foodIntroduce: NSString = food.intro
            
            //告诉boundingRectWithSize 正文放置在一个多宽的长方形中，高度不限
            // let maxSize: CGSize = CGSizeMake(UIScreen.mainScreen().bounds.size.width,CGFloat( MAXFLOAT))
            
            //告诉boundingRectWithSize 正文的字体是多大
            let attributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 14)]
            let option = NSStringDrawingOptions.usesLineFragmentOrigin
            let text: NSString = NSString(cString: foodIntroduce.cString(using: String.Encoding.utf8.rawValue)!,
                                          encoding: String.Encoding.utf8.rawValue)!
            let intrRect: CGRect = text.boundingRect(with: CGSize(width: UIScreen.main.bounds.width, height: CGFloat(MAXFLOAT)), options: option, attributes: attributes, context: nil)
            //计算正文占多大的长方形
            
            cih = intrRect.size.height;
        }
        //最后计算frame值
        foodIntroduce.frame = CGRect(x: cix, y: ciy, width: ciw, height: cih)
        
    }
    class func cellHeightWithPostinModel(_ food: ROFoodModel) -> CGFloat{
        var cih: CGFloat = 0
        //得到正文
        let foodIntroduce: NSString = food.intro
        //告诉boundingRectWithSize 正文放置在一个多宽的长方形中，高度不限
        // let maxSize: CGSize = CGSizeMake(UIScreen.mainScreen().bounds.size.width,CGFloat( MAXFLOAT))
        
        //告诉boundingRectWithSize
        //正文的字体是多大
        let attributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 14)]
        let option = NSStringDrawingOptions.usesLineFragmentOrigin
        let text: NSString = NSString(cString: foodIntroduce.cString(using: String.Encoding.utf8.rawValue)!,
                                      encoding: String.Encoding.utf8.rawValue)!
        let intrRect: CGRect = text.boundingRect(with: CGSize(width: UIScreen.main.bounds.width, height: CGFloat(MAXFLOAT)), options: option, attributes: attributes, context: nil)
        //计算正文占多大的长方形
        
        cih = intrRect.size.height;
        
        return cih+20+16;
        
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
