//
//  ROFoodListTableViewController.swift
//  LuYao
//
//  Created by luyao on 2017/4/22.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class ROFoodListTableViewController: UITableViewController {
    var shop: ROShopModel!{ //商店模型
        didSet(oldValue){
            shopId = shop.shop_id
            shopName = shop.shopname
        }
    }
    
    var shopId: NSNumber!
    var shopName: NSString!
    var foodList: NSArray = NSArray()
    var collectionBarBtnItem: UIBarButtonItem!//右侧按钮
    var isCollect : NSString!//查看是否收藏
    var rightBtn: UIButton = UIButton.init()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        //导航右按钮，收藏店铺
        rightBtn.sizeToFit()
        rightBtn.addTarget(self, action:#selector(collectionShop), for: UIControlEvents.touchUpInside)
        collectionBarBtnItem = UIBarButtonItem.init(customView: rightBtn)
        self.navigationItem.rightBarButtonItem = collectionBarBtnItem
        self.title = self.shopName as String?
        let parameters: NSDictionary = ["shop_id": shopId] as NSDictionary
        RONetworkMngTool.sharedNetWorkMngTool().RONetwork_GetFoodListParameters(parameters, view: self.view, block: { (foodList) -> Void in
            self.foodList = foodList
            self.tableView.reloadData()
        })
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    override func viewWillAppear(_ animated: Bool) {
        
        isCollected()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.foodList.count == 0 {
            return 0
        }
        return self.foodList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "foodCell", for: indexPath)
        if self.foodList.count != 0 {
            let food: ROFoodModel = self.foodList[(indexPath as NSIndexPath).row] as! ROFoodModel
            cell.textLabel!.text = food.foodname as String
            let imagestr = food.pic as String
            let imageUrl = URL(string: imagestr)
            cell.imageView!.setImageWith(imageUrl, placeholderImage: UIImage(named: "imageViewHolder"))
            cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        }
        
        
        
        return cell
    }
    //跳转到fooddetail页面 选中的food值是一个foodmodel类型的对象 将选中的food值传到下一个页面
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentFood: ROFoodModel = foodList[(indexPath as NSIndexPath).row] as! ROFoodModel
        self.performSegue(withIdentifier: "tofoodDetail", sender: currentFood)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let foodDetailController: ROFoodDetailTableViewController = segue.destination as! ROFoodDetailTableViewController
        foodDetailController.food = sender as! ROFoodModel
    }
    func collectionShop (){
        //下面方法完成两个功能，如果之前已经收藏，那么次函数的功能就是取消收藏；如果之前没有收藏，那么此函数的功能就是添加收藏
        let parameters2: NSDictionary = ["user_id": userId,"shop_id": self.shop.shop_id] as NSDictionary
        RONetworkMngTool.sharedNetWorkMngTool().RONetwork_collectShopWithParameters(parameters2, view: self.view) { (success) in
            if success == "1" {
                self.isCollected()
            } else {
                
            }
        }
    }
    //判断之前是否收藏
    func isCollected() {
        
        let parameters: NSDictionary = ["user_id": userId,"shop_food_id": self.shopId,"flag": 0] as NSDictionary
        RONetworkMngTool.sharedNetWorkMngTool().RONetwork_isCollectedWithParameters(parameters, view: self.view) { (isCollect) in
            self.isCollect = isCollect
            
            if self.isCollect == "0" {
                self.rightBtn.setImage(UIImage.init(named: "unlike.png"), for: UIControlState.normal)
            }else if self.isCollect == "1" {
                self.rightBtn.setImage(UIImage.init(named: "like.png"), for: UIControlState.normal)
            }
            
        }
    }
    
}
