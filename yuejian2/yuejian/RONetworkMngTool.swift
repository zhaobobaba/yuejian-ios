//
//  RONetworkMngTool.swift
//  yuejian
//
//  Created by yuejian on 2017/9/29.
//  Copyright © 2017年 yuejian. All rights reserved.
//

import UIKit
var userId : NSString!
let single = RONetworkMngTool()
class RONetworkMngTool: NSObject {
    
    
    class func sharedNetWorkMngTool()-> RONetworkMngTool {
        return single
    }
    
    func RONetwork_Logon(_ parameters: NSDictionary, view: UIView, block: @escaping (_ flag: NSString) -> Void)  {
        //      MBProgressHUD.showAdded(to: view, animated: true)
        let afManager = AFHTTPRequestOperationManager()
        afManager.get("http://60.205.189.39/userLogin.do",parameters: parameters, success: {(Operation:AFHTTPRequestOperation?, responseObjct:Any?) in
            MBProgressHUD.hide(for: view, animated: true)
            //                    print(responseObjct)
            //                    let userid = (responseObjct as! NSDictionary).object(forKey: "userid") as! NSString
            //                    if userid != "0"{
            //                    print("登录成果")
            //                    }else{
            //                        print("登录失败")
            //                    }
            let logoReturnModel = ROLogonReturnModel.logonReturnModelWithDict(responseObjct as! NSDictionary)
            //当你成功登陆后保存用户名和密码
            if logoReturnModel.userid != "0" {
                userId = logoReturnModel.userid
                //print("登录成功")
                block("1")
            }else {
                MBProgressHUD.showDelayHUDToView(view, message:logoReturnModel.msg as String)
                //print("登录失败")
                block("0")
            }
            
        }, failure:{ (Operation: AFHTTPRequestOperation?, error:Error?) in
            MBProgressHUD.hide(for: view, animated: true)
            MBProgressHUD.showDelayHUDToView(view, message: "网络故障，请重试")
            block("0")
        })
        
    }
    func RegistNetwork_Logon(_ parameters: NSDictionary, view: UIView, block: @escaping (_ flag: NSString) -> Void)  {
        //      MBProgressHUD.showAdded(to: view, animated: true)
        let afManager = AFHTTPRequestOperationManager()
        afManager.get("http://60.205.189.39/userRegister.do",parameters: parameters, success: {(Operation:AFHTTPRequestOperation?, responseObjct:Any?) in
            MBProgressHUD.hide(for: view, animated: true)
            let RegReturnModel = RegistReturnModel.RegReturnModelWithDict(responseObjct as! NSDictionary)
            if RegReturnModel.success != "0" {
                //print("注册成功")
                block("1")
            }else {
                MBProgressHUD.showDelayHUDToView(view, message:RegReturnModel.msg as String)
                //print("注册失败")
                block("0")
            }
            
        }, failure:{ (Operation: AFHTTPRequestOperation?, error:Error?) in
            MBProgressHUD.hide(for: view, animated: true)
            MBProgressHUD.showDelayHUDToView(view, message: "网络故障，请重试")
            block("0")
        })
        
    }
   
}


