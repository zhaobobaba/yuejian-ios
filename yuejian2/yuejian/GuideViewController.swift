//
//  GuideViewController.swift
//  yuejian
//
//  Created by yuejian on 2017/9/29.
//  Copyright © 2017年 yuejian. All rights reserved.
//

import UIKit

class GuideViewController: UIViewController,UIScrollViewDelegate {
    
    var numOfPages = 3
    override func viewDidLoad()
    {
        let frame = self.view.bounds
        //初始化
        let scrollView=UIScrollView()
        scrollView.frame=self.view.bounds
        scrollView.delegate = self
        //让内容横向滚动，设置横向内容宽度为3个页面的宽度总和
        scrollView.contentSize=CGSize(width: frame.size.width*CGFloat(numOfPages),height: frame.size.height)
        
        scrollView.isPagingEnabled=true
        scrollView.showsHorizontalScrollIndicator=false
        scrollView.showsVerticalScrollIndicator=false
        scrollView.scrollsToTop=false
        
        for i in 0..<numOfPages{
            let imgfile = "lu\(Int(i+1)).jpg"
            print(imgfile)
            let image = UIImage(named:"\(imgfile)")
            let imgView = UIImageView(image: image)
            imgView.frame=CGRect(x: frame.size.width*CGFloat(i),y: CGFloat(0),width: frame.size.width,height: frame.size.height)
            scrollView.addSubview(imgView)
        }
        
        scrollView.contentOffset = CGPoint.zero
        self.view.addSubview(scrollView)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        print("scrolled:\(scrollView.contentOffset)")
        let thirdh = CGFloat(numOfPages-1) * self.view.bounds.size.width
        if(scrollView.contentOffset.x > thirdh)
        {
            let mainStoryboard = UIStoryboard(name:"Main", bundle:nil)
            let viewController: ViewController = mainStoryboard.instantiateViewController(withIdentifier: "mainstoryboard") as! ViewController
            self.present(viewController, animated: true, completion:nil)
            
        }
    }
}
