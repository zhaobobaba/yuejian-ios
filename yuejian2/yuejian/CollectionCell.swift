//
//  CollectionCell.swift
//  LuYao
//
//  Created by luyao on 2017/5/27.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class CollectionCell: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        self.imageView!.frame = CGRect(x: 8, y: 4, width: 100, height: 72)
        var tmpframe = self.textLabel!.frame
        tmpframe.origin.x = 116
        self.textLabel!.frame = tmpframe
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
