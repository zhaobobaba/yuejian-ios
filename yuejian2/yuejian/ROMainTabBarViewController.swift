//
//  ROMainTabBarViewController.swift
//  LuYao
//
//  Created by luyao on 2017/4/22.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class ROMainTabBarViewController: RDVTabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTabBarController()
        // Do any additional setup after loading the view.
    }
    
    func setupTabBarController(){
        //        let firstViewController = UIViewController()
        //        firstViewController.title = "店铺"
        //        firstViewController.view.backgroundColor = UIColor.red
        //        let firstNavigationController = UINavigationController(rootViewController: firstViewController)
        
        let homePageStoryboard: UIStoryboard = UIStoryboard(name: "homePage", bundle: nil)
        let homePageNaigationController : UIViewController = homePageStoryboard.instantiateViewController(withIdentifier: "homePageNav")
        
        //        let secondViewController = UIViewController()
        //        secondViewController.title = "收藏"
        //        secondViewController.view.backgroundColor = UIColor.blue
        //        let secondNavigationController = UINavigationController(rootViewController: secondViewController)
        
        let CollectionStoryboard: UIStoryboard = UIStoryboard(name: "Collection", bundle: nil)
        let CollectionNaigationController : UIViewController = CollectionStoryboard.instantiateViewController(withIdentifier: "collection")
        
        
        
        let searchStoryboard: UIStoryboard = UIStoryboard(name: "search", bundle: nil)
        let searchNaigationController : UIViewController = searchStoryboard.instantiateViewController(withIdentifier: "search")
        
        //        let thirdViewController = UIViewController()
        //        thirdViewController.title = "我"
        //        thirdViewController.view.backgroundColor = UIColor.yellow
        //        let thirdNavigationController = UINavigationController(rootViewController: thirdViewController)
        
        let myselfStoryboard: UIStoryboard = UIStoryboard(name: "myself", bundle: nil)
        let myselfNaigationController : UIViewController = myselfStoryboard.instantiateViewController(withIdentifier: "me")
        
        self.viewControllers = [homePageNaigationController,CollectionNaigationController,searchNaigationController,myselfNaigationController]
        
        let tabBarItemImages = ["first","second","third","fourth"]
        let tabBarItemTitles = ["店铺","收藏","搜索","我"]
        
        let tabBar = self.tabBar
        tabBar?.frame = CGRect(x: (tabBar?.frame.minX)!, y: (tabBar?.frame.minY)!, width: (tabBar?.frame.width)!, height: 63)
        var index : Int = 0
        for item in self.tabBar.items as! [RDVTabBarItem]{
            let selectedimage = UIImage(named: "\(tabBarItemImages[index])"+"_select")
            let unselectedimage = UIImage(named: "\(tabBarItemImages[index])"+"_unselect")
            
            item.setFinishedSelectedImage(selectedimage, withFinishedUnselectedImage: unselectedimage)
            item.title = tabBarItemTitles[index]
            index += 1
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
