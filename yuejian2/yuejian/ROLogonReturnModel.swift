//
//  ROLogonReturnModel.swift
//  yuejian
//
//  Created by yuejian on 2017/9/29.
//  Copyright © 2017年 yuejian. All rights reserved.
//

import UIKit

class ROLogonReturnModel: NSObject {        
        var msg: NSString
        var userid: NSString!
        static var userName:String!
        static var userPwd:String!
        static var address:String!
        static var mobilenum:String!
        
        override init () {
            userid = NSString()
            msg = NSString()
        }
        class func logonReturnModelWithDict(_ dict: NSDictionary ) -> ROLogonReturnModel {
            
            let model = ROLogonReturnModel()
            //根据字典，进行模型的初始化
            let userid = dict.object(forKey: "userid") as! NSString
            model.userid = userid
            
            if userid == "0" {
                let msg = "用户名或者密码错误"
                model.msg = msg as NSString
            }
            
            return model
        }
        
        class func getUserInformation(){
            let urlStr = "http://60.205.189.39/getUserById.do"
            let paraDict = ["user_id":userId.intValue]
            AFHTTPRequestOperationManager().post(urlStr, parameters: paraDict, success: { (response:AFHTTPRequestOperation?, data:Any?) in
                let dict = data as! NSDictionary
                self.userName = dict["username"] as! String
                self.userPwd = dict["userpass"] as! String
                self.address = dict["address"] as! String
                self.mobilenum = dict["mobilenum"] as! String
            }) { (response:AFHTTPRequestOperation?, error:Error?) in
                print(response!.debugDescription)
            }
        }
        
}
