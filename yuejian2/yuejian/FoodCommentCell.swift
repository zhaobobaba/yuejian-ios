//
//  FoodCommentCell.swift
//  LuYao
//
//  Created by luyao on 2017/5/24.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class FoodCommentCell: UITableViewCell {
    var foodName:UILabel!
    var price:UILabel!
    var shopName:UILabel!
    var userName:UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        foodName = UILabel()
        foodName.font = UIFont.systemFont(ofSize: 16)
        self.contentView.addSubview(foodName)
        
        price = UILabel()
        price.font = UIFont.systemFont(ofSize: 16)
        self.contentView.addSubview(price)
        
        shopName = UILabel()
        shopName.textAlignment = .left
        shopName.font = UIFont.systemFont(ofSize: 16)
        self.contentView.addSubview(shopName)
        
        userName = UILabel()
        userName.textAlignment = .center
        userName.font = UIFont.systemFont(ofSize: 16)
        self.contentView.addSubview(userName)
        textLabel?.font = UIFont.systemFont(ofSize: 16)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        textLabel?.frame = CGRect(x:10,y:0,width:UIScreen.main.bounds.width,height:30)
        foodName.frame = CGRect(x: 10, y: 30, width: 80, height: 50)
        price.frame = CGRect(x: 100, y: 32, width: 80, height: 30)
        shopName.frame = CGRect(x: 190, y: 32, width: 120, height: 30)
        userName.frame = CGRect(x: 10, y: 60, width: Int(ScreenWidth), height: 20)
        
        // Configure the view for the selected state
    }
    
           

}
