//
//  RegistReturnModel.swift
//  yuejian
//
//  Created by yuejian on 2017/9/29.
//  Copyright © 2017年 yuejian. All rights reserved.
//

import UIKit

class RegistReturnModel: NSObject {
    var success: NSString
    var msg: NSString
    override init () {
        success = NSString()
        msg = NSString()
    }
    class func RegReturnModelWithDict(_ dict: NSDictionary ) ->RegistReturnModel {
        let model = RegistReturnModel()
        //根据字典，进行模型的初始化
        let success = dict.object(forKey: "success") as! NSString
        model.success = success
        
        if success == "0" {
            let msg = " 用户名已存在，请重试"
            model.msg = msg as NSString
        }
        return model
    }
}
