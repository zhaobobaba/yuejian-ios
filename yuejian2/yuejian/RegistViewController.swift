//
//  RegistViewController.swift
//  yuejian
//
//  Created by yuejian on 2017/9/29.
//  Copyright © 2017年 yuejian. All rights reserved.
//

import UIKit

class RegistViewController: UIViewController {
    @IBOutlet weak var userpass: UITextField!
    
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var mobilenum: UITextField!
    @IBAction func Back(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    @IBOutlet weak var address: UITextField!
    @IBOutlet weak var comment: UITextField!
    
    @IBAction func Regist(_ sender: UIButton) {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        if username.text == ""{
            print("请输入用户名！")
            MBProgressHUD.showDelayHUDToView(self.view, message: "请输入用户名！")
        }else {
            if userpass.text == ""{
                print("请输入密码！")
                MBProgressHUD.showDelayHUDToView(self.view, message: "请输入密码！")
            }else{
                if mobilenum.text == ""{
                    print("请输入密码！")
                    MBProgressHUD.showDelayHUDToView(self.view, message: "请输入手机号！")
                }else{
                    if address.text == ""{
                        print("请输入密码！")
                        MBProgressHUD.showDelayHUDToView(self.view, message: "请输入地址！")
                    }else{
                        //验证用户名和密码
                        let parameters: NSDictionary = ["username": username.text!,"userpass": userpass.text!,"mobilenum": mobilenum.text!,"address": address.text!,"comment": comment.text!]
                        RONetworkMngTool.sharedNetWorkMngTool().RegistNetwork_Logon(parameters, view: self.view, block: {(flag)in
                            if flag == "1" {
                                print("注册成功，跳转主页")
                            }else{
                                print("注册失败")
                            }
                        })
                    }
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
