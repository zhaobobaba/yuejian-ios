//
//  ROLogonViewController.swift
//  yuejian
//
//  Created by yuejian on 2017/9/29.
//  Copyright © 2017年 yuejian. All rights reserved.
//

import UIKit

class ROLogonViewController: UIViewController {

    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var passWord: UITextField!
    @IBAction func logon(_ sender: UIButton) {
        //取得用户名和密码，使用keychain保存用户名和密码
        if userName.text == "" {
            //显示提示，调用MBProgressHUD的扩展功能
            //print("用户名没有输入")
            MBProgressHUD.showDelayHUDToView(self.view, message: "用户名没有输入")
            
        } else {
            if passWord.text == "" {
                MBProgressHUD.showDelayHUDToView(self.view, message: "密码没有输入")
            } else {
                let parameters: NSDictionary = ["username": userName.text!,"userpass": passWord.text!] as NSDictionary
                RONetworkMngTool.sharedNetWorkMngTool().RONetwork_Logon(parameters, view: self.view, block: { (flag) -> Void in
                    if flag == "1" {
                        //本地存储用户的其他信息
                        ROLogonReturnModel.getUserInformation()
                        
                        let _ = (ACSimpleKeychain.defaultKeychain() as AnyObject).storeUsername(self.userName.text, password: self.passWord.text, identifier: "user1", forService: "userpassword")
                        //登陆到主界面
                        //测试暂时先把这句去掉
                      
                    } else {
                        //清空用户名和密码
                        let _ = (ACSimpleKeychain.defaultKeychain() as AnyObject).deleteAllCredentials(forService: "userpassword")
                        let _ =  (ACSimpleKeychain.defaultKeychain() as AnyObject).storeUsername("", password: "", identifier: "user1", forService: "userpassword")
                    }
                })
                
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        featchUserNamePwd()
        // Do any additional setup after loading the view.
    }
    func featchUserNamePwd(){
        let userPwdDict = (ACSimpleKeychain.defaultKeychain() as AnyObject).credentials(forIdentifier: "user1", service: "userpassword") as NSDictionary
        if userPwdDict.count != 0 {
            userName.text = userPwdDict.object(forKey: "username") as? String
            passWord.text = userPwdDict["password"] as? String
        }
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.userName.resignFirstResponder()
        self.passWord.resignFirstResponder()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}
