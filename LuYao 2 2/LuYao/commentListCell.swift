//
//  commentListCell.swift
//  LuYao
//
//  Created by luyao on 2017/6/7.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit
protocol commentListDelegate {
    func alterComment(indexPath:IndexPath,block:@escaping (_ content:String)->Void)
    func deleteComment(indexPath:IndexPath)
}

class commentListCell:UITableViewCell{
    var foodNameLabel:UILabel!
    var priceLabel:UILabel!
    var shopNameLabel:UILabel!
    var indexPath:IndexPath!
    var comment_time:String!
    var commentDelegate:commentListDelegate!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setView()
    }
    
    private func setView(){
        let alterBtn = UIButton(frame: CGRect(x: UIScreen.main.bounds.width-60, y: 10, width: 50, height: 35))
        alterBtn.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        alterBtn.setTitle("修改", for: .normal)
        alterBtn.backgroundColor = UIColor.yellow
        alterBtn.addTarget(self, action: #selector(alter), for: .touchUpInside)
        self.addSubview(alterBtn)
        
        let deleteBtn = UIButton(frame: CGRect(x: UIScreen.main.bounds.width-60, y: 55, width: 50, height: 35))
        deleteBtn.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        deleteBtn.setTitle("删除", for: .normal)
        deleteBtn.backgroundColor = UIColor.yellow
        deleteBtn.addTarget(self, action: #selector(delete_Comment), for: .touchUpInside)
        self.addSubview(deleteBtn)
        
        foodNameLabel = UILabel(frame: CGRect(x: 5, y: 40, width: 120, height: 40))
        foodNameLabel.font = UIFont.systemFont(ofSize: 20)
        self.addSubview(foodNameLabel)
        
        priceLabel = UILabel(frame: CGRect(x: 125, y: 42, width: 120, height: 40))
        priceLabel.font = UIFont.systemFont(ofSize: 18)
        self.addSubview(priceLabel)
        
        shopNameLabel = UILabel(frame: CGRect(x: 120, y: 75, width: 120, height: 25))
        shopNameLabel.font = UIFont.systemFont(ofSize: 16)
        shopNameLabel.textAlignment = .center
        self.addSubview(shopNameLabel)
        
        let line = CAShapeLayer()
        line.fillColor = UIColor.lightGray.cgColor
        line.path = UIBezierPath(rect: CGRect(x: 0, y: 99, width: UIScreen.main.bounds.width, height: 1)).cgPath
        self.layer.addSublayer(line)
    }
    
    @objc private func alter(){
        commentDelegate.alterComment(indexPath: indexPath) { (content) in
            self.textLabel?.text = content + "(\(self.comment_time!))"
        }
    }
    
    @objc private func delete_Comment(){
        commentDelegate.deleteComment(indexPath: indexPath)
    }
    
    override func layoutSubviews() {
        textLabel?.frame = CGRect(x: 5, y: 0, width: 200, height: 40)
        textLabel?.font = UIFont.systemFont(ofSize: 20)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
