//
//  DetailQuestionUserAnswerTableViewCell.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/1.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class DetailQuestionUserAnswerTableViewCell: UITableViewCell {
    
    @IBOutlet weak var timee: UILabel!
    
    @IBOutlet weak var testt: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    class func creatCell(_ tableView : UITableView) -> DetailQuestionUserAnswerTableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "DetailQuestionUserAnswerTableViewCell")
        if cell == nil{
            cell = (Bundle.main.loadNibNamed("DetailQuestionUserAnswerTableViewCell", owner: nil, options: nil))?.first as! DetailQuestionUserAnswerTableViewCell
        }
        return cell as! DetailQuestionUserAnswerTableViewCell;
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

