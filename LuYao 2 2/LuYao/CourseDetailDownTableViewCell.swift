//
//  CourseDetailDownTableViewCell.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/1.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class CourseDetailDownTableViewCell: UITableViewCell {
    
    @IBOutlet weak var tel: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var notice: UILabel!
    @IBOutlet weak var test1: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    class func creatCell(_ tableView:UITableView)->CourseDetailDownTableViewCell{
        var cell = tableView.dequeueReusableCell(withIdentifier: "CourseDetailDownTableViewCell")
        if cell == nil{
            cell = (Bundle.main.loadNibNamed("CourseDetailDownTableViewCell", owner: nil, options: nil))?.first as! CourseDetailDownTableViewCell
        }
        return cell as! CourseDetailDownTableViewCell
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

