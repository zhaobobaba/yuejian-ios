//
//  CourseModel.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/1.
//  Copyright © 2017年 luyao. All rights reserved.
//

import Foundation
import UIKit

class CourseModel: NSObject {
    
    
    var id:NSString!
    var name:NSString!
    var thumb:NSString!
    var thumbsize:NSString!
    var degree:NSString!
    var intro:NSString!
    var notice:NSString!
    var bmnotic:NSString!
    var traintime:NSString!
    var place:NSString!
    var price:NSString!
    var tel:NSString!
    var userid:NSString!
    var hits:NSString!
    var insert_time:NSNull!
    var update_time:NSString!
    var check_userid:NSString!
    var checktime:NSNull!
    var checkstate:NSString!
    var listorder:NSString!
    var vstate:NSString!
    
    
    
    
    
    
    class func CourseModelWithDict(_ dict:NSDictionary)-> CourseModel{
        let model = CourseModel()
        
        model.id = dict.value(forKey: "id") as! NSString!
        model.name = dict.value(forKey: "name") as! NSString!
        model.thumb = dict.value(forKey: "thumb") as! NSString!
        model.thumbsize = dict.value(forKey: "thumbsize") as! NSString!
        model.intro = dict.value(forKey: "intro") as! NSString!
        model.notice = dict.value(forKey: "notice") as! NSString!
        model.bmnotic = dict.value(forKey: "bmnotic") as! NSString!
        model.hits = dict.value(forKey: "hits") as! NSString!
        model.traintime = dict.value(forKey: "traintime") as! NSString!
        model.degree = dict.value(forKey: "degree") as! NSString!
        model.userid = dict.value(forKey: "userid") as! NSString!
        model.insert_time = dict.value(forKey: "insert_time") as! NSNull!
        model.update_time = dict.value(forKey: "update_time") as! NSString!
        model.check_userid = dict.value(forKey: "check_userid") as! NSString!
        model.checktime = dict.value(forKey: "checktime") as! NSNull!
        model.checkstate = dict.value(forKey: "checkstate") as! NSString!
        model.listorder = dict.value(forKey: "listorder") as! NSString!
        model.vstate = dict.value(forKey: "vstate") as! NSString!
        model.place = dict.value(forKey: "place") as! NSString!
        model.price = dict.value(forKey: "price") as! NSString!
        model.tel = dict.value(forKey: "tel") as! NSString!
        
        
        
        
        return model
    }
    
    
    
}

