//
//  CourseVideTableViewCell.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/6.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class CourseVideTableViewCell: UITableViewCell {
    @IBOutlet weak var tittle: UILabel!
    @IBOutlet weak var thumbb: UIImageView!
    
    @IBOutlet weak var Btn: UIButton!
    @IBOutlet weak var num: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    class func creatCell(_ tableView:UITableView)->CourseVideTableViewCell{
        var cell = tableView.dequeueReusableCell(withIdentifier: "CourseVideTableViewCell")
        if cell == nil{
            cell = (Bundle.main.loadNibNamed("CourseVideTableViewCell", owner: nil, options: nil))?.first as! CourseVideTableViewCell
        }
        return cell as! CourseVideTableViewCell
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

