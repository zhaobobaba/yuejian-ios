//
//  VideoTableViewCell.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/1.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class SingleView: UIView {
    var pic:UIImageView!
    var Btn:UIButton!
    var Tittle:UILabel!
    var darksView:UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        creatView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func creatView(){
        
        pic = UIImageView(frame: self.bounds)
        self.addSubview(pic)
        
        
        darksView = UIView(frame: self.bounds)
        darksView.backgroundColor = UIColor(red: 0 / 255.0, green: 0 / 255.0, blue: 0 / 255.0, alpha: 0.2)
        self.addSubview(darksView)
        
        
        
        Btn = UIButton(frame: CGRect(x: self.frame.size.width/5, y: self.frame.size.height/4, width: self.frame.size.width*3/5, height: self.frame.size.height/3))
        Btn.setTitle("查看详情", for: .normal)
        Btn.setTitleColor(UIColor.white, for: .normal)
        Btn.backgroundColor = UIColor(displayP3Red: 0, green: 0, blue: 0, alpha: 0)
        Btn.layer.borderWidth = 1
        Btn.layer.borderColor = UIColor.white.cgColor
        Btn.layer.cornerRadius = 10
        
        self.addSubview(Btn)
        
        Tittle = UILabel(frame: CGRect(x: self.frame.size.width/5, y: self.frame.size.height*3/4, width: self.frame.size.width*3/5, height: self.frame.size.height/4))
        Tittle.font = UIFont.systemFont(ofSize: 16)
        Tittle.textColor = UIColor.white
        
        self.addSubview(Tittle)
        
    }
    
}

