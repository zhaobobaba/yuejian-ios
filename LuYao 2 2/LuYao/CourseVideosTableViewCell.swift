//
//  CourseVideosTableViewCell.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/6.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class CourseVideosTableViewCell: UITableViewCell,UIScrollViewDelegate {

    let radio:CGFloat = 0.2
    let width = UIScreen.main.bounds.width
    let height = UIScreen.main.bounds.size.height
    var number = 0{
        didSet(oldvalue){
            scr?.contentSize = CGSize(width: width*2*CGFloat(number)/3, height:height*radio*3/4)
            creatScrollView()
        }
    }
    var TT:UILabel!
    var scr:UIScrollView!
    var imageViews = NSMutableArray()
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        scr = UIScrollView.init(frame: CGRect(x: 0, y:height*radio/4, width: width, height: height*radio*3/4))
        scr?.delegate = self
        scr?.isScrollEnabled = true
        scr.bounces = false
        scr.alwaysBounceVertical = false
        scr.showsVerticalScrollIndicator = false
        
        
        self.addSubview(scr!)
        
        TT = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: height*radio/4))
        TT.font = UIFont.systemFont(ofSize: 16)
        self.addSubview(TT)
        
    }
    func creatScrollView(){
        
        for i in 0..<number{
            
            let View = SingleView.init(frame: CGRect(x: scr.frame.size.width*CGFloat((1+5*i))/8, y: scr.frame.size.height/8, width: scr.frame.size.width/2, height: scr.frame.size.height*3/4))
            
            scr?.addSubview(View)
            
            imageViews.add(View)
            
            
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
