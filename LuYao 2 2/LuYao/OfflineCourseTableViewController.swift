//
//  OfflineCourseTableViewController.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/1.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class OfflineCourseTableViewController: UITableViewController {
    
    var Course:CourseModel!
    var turebarbtnItem:UIBarButtonItem!
    var rightBtn:UIButton = UIButton.init()
    override func viewDidLoad() {
        
        super.viewDidLoad()
        automaticallyAdjustsScrollViewInsets = false
        
        rightBtn.sizeToFit()
        rightBtn.addTarget(self, action: #selector(Ifcollect), for: UIControlEvents.touchUpInside)
        turebarbtnItem = UIBarButtonItem.init(customView: rightBtn)
        self.navigationItem.rightBarButtonItem = turebarbtnItem
        self.rightBtn.setImage(UIImage.init(named: "buzai.png"), for: .normal)
        CreatFootView()
        
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var basecell = UITableViewCell()
        if indexPath.section == 0{
            let cell = CourseDetailTableViewCell.creatCell(tableView)
            let imageStr = "http://172.24.10.175/workout/Uploads/" + (Course.thumb! as String)
            let imageUrl = URL(string: imageStr)
            let data = try! Data(contentsOf: imageUrl!)
            let image = UIImage(data: data)
            cell.thumbb.image = image
            
            var type:String!
            if Course.degree == "1"{
                type = "入门"
            }else if Course.degree == "2"{
                type = "菜鸟"
            }else if Course.degree == "3"{
                type = "进阶"
            }
            cell.adress.text = type
            basecell = cell
        }
        else if indexPath.section == 1{
            let cell = CourseDetailDownTableViewCell.creatCell(tableView)
            
            
            cell.test1.text = Course.intro as String
            
            cell.notice.text = Course.notice as String
            
            cell.price.text = Course.price as String
            cell.time.text = Course.traintime as String
            cell.tel.text = Course.tel as String
            basecell = cell
        }
        
        
        basecell.selectionStyle = UITableViewCellSelectionStyle.none
        return basecell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return height * 0.3
        }else if indexPath.section == 1 || indexPath.section == 2 {
            return height * 0.1
        }else {
            return height * 0.15
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        return 8
        
    }
    
    func Ifcollect(){
        
    }
    
    func CreatFootView(){
        let footerView:UIView = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 40))
        let footButton:UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: width, height: 40))
        footButton.setTitle("加入训练", for: .normal)
        footButton.setTitleColor(UIColor.white, for: .normal)
        footButton.backgroundColor = UIColor.orange
        footButton.addTarget(self, action: #selector(AddCourse), for: .touchUpInside)
        footerView.addSubview(footButton)
        tableView.tableFooterView = footerView
        
    }
    func AddCourse(){
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 4
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }
}




