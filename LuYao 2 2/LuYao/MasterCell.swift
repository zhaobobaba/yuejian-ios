//
//  MasterCell.swift
//  LuYao
//
//  Created by Bobobo on 2017/10/24.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

protocol masterListDelegate {
    
}
class masterListCell:UITableViewCell{
    var masterDelegate:masterListDelegate!
    var indexPath:IndexPath!
    var Realname:UILabel!
    var Actcate_name:UILabel!
    var Introduction:UILabel!
    var MasterPhoto:UIImageView!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setView(){
        MasterPhoto = UIImageView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 200))
        self.addSubview(MasterPhoto)
        
        Actcate_name = UILabel(frame: CGRect(x: 180, y: 10, width: 100, height: 40))
        Actcate_name.font = UIFont.systemFont(ofSize: 20)
        self.addSubview(Actcate_name)
        
        Introduction = UILabel(frame: CGRect(x: 240, y: 10, width: 160, height: 40))
        Introduction.font = UIFont.systemFont(ofSize: 20)
        self.addSubview(Introduction)
    }
    
    override func layoutSubviews() {
        textLabel?.frame = CGRect(x: 10, y: 5, width: 120, height: 40)
        textLabel?.font = UIFont.systemFont(ofSize: 20)
    }
    
}



