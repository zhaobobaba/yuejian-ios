//
//  SearchViewController.swift
//  LuYao
//
//  Created by bai on 2017/5/27.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate{
    var tableView:UITableView!
    var searchBar:UISearchBar!
    var dataSource:NSArray!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setView()
    }
    
    func setView(){
        tableView = UITableView(frame: CGRect(x: 0, y: 20, width: view.bounds.width, height: view.bounds.height-64))
        tableView.backgroundColor = UIColor.white
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.delegate = self
        tableView.dataSource = self
        view.addSubview(tableView)
        
        searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: 40))
        searchBar.backgroundColor = UIColor.white
        searchBar.searchBarStyle = .minimal
        searchBar.delegate = self
        searchBar.barStyle = .default
        searchBar.showsCancelButton = true
        
        let cancleBtn = searchBar.value(forKey: "cancelButton") as! UIButton
        cancleBtn.setTitle("搜索", for: .normal)
        
        tableView.tableHeaderView = searchBar
    }
    
    func search(){
        let paraDict = ["search":searchBar.text!] as NSDictionary
        RONetworkMngTool.sharedNetWorkMngTool().RONetwork_searchFoodListParameters(paraDict, view: self.view) { (searchArray) in
            self.dataSource = searchArray as  NSArray
            self.tableView.reloadData()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        search()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataSource != nil{
            return dataSource.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentFood: ROFoodModel = dataSource[(indexPath as NSIndexPath).row] as! ROFoodModel
        self.performSegue(withIdentifier: "toFoodDetail", sender: currentFood)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let foodDetailController: ROFoodDetailTableViewController = segue.destination as! ROFoodDetailTableViewController
        foodDetailController.food = sender as! ROFoodModel
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for:indexPath)
        let dict = dataSource[indexPath.row] as! ROFoodModel
        cell.textLabel?.text = dict.foodname as String
        return cell
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        searchBar.resignFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

