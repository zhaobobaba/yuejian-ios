//
//  PicAddCommitTableViewController.swift
//  LuYao
//
//  Created by 路瑶 on 2017/10/20.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class PicAddViewController: UIViewController,UITextViewDelegate,UIImagePickerControllerDelegate,
UINavigationControllerDelegate{
    
    
    @IBOutlet weak var intro: UITextField!
    //sessionID存的是什么ID？
    @IBAction func ok(_ sender: UIButton) {
        let paramters = ["mod":"shaitu","SessionID":ROLogonReturmModel.sessionID,"title":intro.text!,"userid":ROLogonReturmModel.userid,"club_id":5] as NSDictionary
        RONetworkMngTool.sharedNetwotkmngTool().Network_ShaiTu(paramters, image: upimage.image!){
            (flag) in
            _ = self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    @IBAction func addpic(_ sender: UIButton) {
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            //初始化图片控制器
            let picker = UIImagePickerController()
            //设置代理
            picker.delegate = self
            //指定图片控制器类型
            picker.sourceType = .photoLibrary
            //弹出控制器，显示界面
            self.present(picker, animated: true, completion: {
                () -> Void in
            })
        }else{
            print("读取相册错误")
        }
        
        
    }
    
    @IBOutlet weak var upimage: UIImageView!
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [String : Any]) {
        
        //获取选择的原图
        let pickedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        upimage.image = pickedImage
      
    picker.dismiss(animated: true, completion:nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title  = "添加评论列表"
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
