//
//  TrainModel.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/1.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class ActiveModel: NSObject {
    var id:NSString!
    var rec_id:NSString!
    var rec_mod:NSString!
    var thumb:NSString!
    var thumbsize:NSString!
    var rec_name:NSString!
    var userid:NSString!
    var hits:NSString!
    var insert_time:NSString!
    var update_time:NSString!
    var check_userid:NSString!
    var checktime:NSString!
    var checkstate:NSString!
    var listorder:NSString!
    var vstate:NSString!
    
    class func ActiveModelWithDict(_ dict:NSDictionary) -> ActiveModel{
        let model = ActiveModel()
        model.setValuesForKeys(dict as! [String:NSObject])
        return model
    }
    
    
}


