//
//  ClubDetailTableViewController.swift
//  LuYao
//
//  Created by 路瑶 on 2017/10/16.
//  Copyright © 2017年 luyao. All rights reserved.
//


import UIKit

class ClubDetailTableViewController: UITableViewController {
    
    var club:ClubModel!
    var clubid:NSNumber!
    var clubdetail: ClubdetailModel!
    var flag:Bool = true
    
    var turebarbtnItem:UIBarButtonItem!
    var videosList = NSArray()
    var courseList = NSArray()
    var teacherList = NSDictionary()
    var rightBtn:UIButton = UIButton.init()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = club.club_name as NSString! as String?
        
        getVideo()
        getCourse()
        
        let parameters: NSDictionary = ["id": club.id] as NSDictionary
        
        RONetworkMngTool.sharedNetwotkmngTool().RONetwork_clubdetail(parameters, view: self.view, block: {
            (clubdetail) -> Void in
            print(clubdetail)
            self.clubdetail = clubdetail
            self.tableView.reloadData()
        })
        
        
        
    }
    
    func getVideo(){
        flag = false
        let parameters = ["mod":"club","SessionID":"2ul9sj8vlfofj122e1s98048k2","id":club.id,"map":"Clubvideo","related":1] as NSDictionary
        RONetworkMngTool.sharedNetwotkmngTool().getclubvideoselect(parameters, view: self.view){(videoList) in
            print("clubvideo")
            print(videoList)
            self.videosList = videoList
            self.tableView.reloadData()
        }
        
    }
    //    func getTeacher(){
    //        let parameters = ["mod":"club","SessionID":"kbh9uuh69vi3lgqq5m80g5o1p5","id":club.id,"map":"Clubteacher","related":1] as NSDictionary
    //        RONetworkMngTool.sharedNetwotkmngTool().getclubteacherselect(parameters, view: self.view ){(teacherList) in
    //            self.teacherList = teacherList
    //            self.tableView.reloadData()
    //        }
    //
    //    }
    func getCourse(){
        flag = true
        let parameters = ["mod":"club","SessionID":"2ul9sj8vlfofj122e1s98048k2","id":club.id,"map":"Clubcourse","related":1] as NSDictionary
        RONetworkMngTool.sharedNetwotkmngTool().getclubcourseselect(parameters, view: self.view){(courseList) in
            self.courseList = courseList
            print(courseList)
            print("clubcourse")
            
            self.tableView.reloadData()
        }
        
    }
    
    
    
    func PlayVideo(sender:UIButton){
        
        let model = videosList[sender.tag] as! ClubVideoModel
        let str = "http://172.24.10.175/workout/Uploads/tvideo/video/" + (model.videopath! as String)
        print("路径：http://172.24.10.175/workout/Uploads/tvideo/video/\(model.videopath!)")
        self.performSegue(withIdentifier: "ToPlayVideo", sender: str)
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var basecell:UITableViewCell = UITableViewCell()
        if indexPath.section == 0{
            let cell = ClubDetailTableViewCell.creatCell(tableView)
            if clubdetail != nil{
                cell.info.text = "地址:\(club.province_name!)\(club.city_name!)\(club.district_name!)"
                let imageStr = "http://172.24.10.175/workout/Uploads/" + (club.thumb! as String)
                let imageUrl = URL(string: imageStr)
                let data = try! Data(contentsOf: imageUrl!)
                let image = UIImage(data: data)
                cell.pic.image = image
                
                cell.course.addTarget(self, action: #selector(getCourse), for: .touchUpInside)
                cell.movie.addTarget(self, action: #selector(getVideo), for: .touchUpInside)
                //cell.Information.addTarget(self, action: #selector(getTeacher), for: .touchUpInside)
                basecell = cell
                
            }
        }
        else if indexPath.section == 1{
            if  flag && courseList.count != 0 {
                let cell = CourseTableViewCell.creatCell(tableView)
                let model = self.courseList[(indexPath as NSIndexPath).row] as! ClubcourseModel
                cell.name.text = model.course_name as String!
                let imageStr = "http://172.24.10.175/workout/Uploads/" + (model.thumb! as String)
                let imageUrl = URL(string: imageStr)
                let data = try! Data(contentsOf: imageUrl!)
                let image = UIImage(data: data)
                cell.thumbb.image = image
                basecell = cell
                
            }
            else if !flag && videosList.count != 0 {
                let cell:CourseVideosTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CourseVideosTableViewCell", for: indexPath) as! CourseVideosTableViewCell
                
                cell.number = videosList.count
                cell.TT.text = "相关视频"
                for i in 0..<videosList.count{
                    let model = videosList[i] as! VideoModel
                    let imageStr = "http://172.24.10.175/workout/Uploads/" + (model.thumb! as String)
                    let imageUrl = URL(string: imageStr)
                    let data = try! Data(contentsOf: imageUrl!)
                    let image = UIImage(data: data)
                    (cell.imageViews[i] as! SingleView).pic.image = image
                    
                    (cell.imageViews[i] as! SingleView).Btn.tag = i
                    (cell.imageViews[i] as! SingleView).Btn.addTarget(self, action: #selector(PlayVideo), for: .touchUpInside)
                }
                basecell = cell
                
                
            }
                
                
            else {
                if flag && courseList.count == 0 {
                    let cell = NoNoTableViewCell.creatCell(tableView)
                    cell.bear.image = UIImage(named: "ku3.png")
                    cell.infooo.text = "没有课程信息"
                    basecell = cell
                    
                }
                else if !flag && videosList.count == 0 {
                    
                    let cell = NoNoTableViewCell.creatCell(tableView)
                    cell.bear.image = UIImage(named: "ku111.png")
                    cell.infooo.text = "视频信息没有获取"
                    basecell = cell
                    
                }
            }
        }
        return basecell
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        return 1
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 275
    }
    
}
