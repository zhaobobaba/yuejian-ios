//
//  ROHUD.swift
//  LuYao
//
//  Created by luyao on 2017/4/22.
//  Copyright © 2017年 luyao. All rights reserved.
//


import Foundation
extension MBProgressHUD {
    
    class func showDelayHUDToview(_ view: UIView, message: String) ->Void{
        let HUD = MBProgressHUD(view: view)
        view.addSubview(HUD!)
        HUD?.yOffset = Float( view.frame.size.height * 1.0 / 4.0)
        HUD?.customView = UIImageView(frame: CGRect.zero)
        HUD?.mode = MBProgressHUDMode.customView
        HUD?.labelText = message
        HUD?.show(true)
        HUD?.hide(true, afterDelay: 1)
        
    }
    
    
    
}
