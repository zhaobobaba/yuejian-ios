//
//  PicViewController.swift
//  LuYao
//
//  Created by 路瑶 on 2017/10/19.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit


let WIDTH = UIScreen.main.bounds.width
let HEIGHT = UIScreen.main.bounds.height

class PicViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    
    @IBOutlet weak var addb: UIBarButtonItem!
    
    var collectionView:UICollectionView!
    var list:NSArray?
    lazy var refresh = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewPic()
        
        DispatchQueue.global(qos: .default).async {
            self.getList()
        }
        
    }
    
    func viewPic(){
        
        view.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.itemSize = CGSize(width: WIDTH/2, height: WIDTH/2)
        flowLayout.minimumInteritemSpacing = 0
        flowLayout.minimumLineSpacing = 0
        //照片展示
        collectionView = UICollectionView(frame: CGRect(x:0,y:20,width:WIDTH,height:HEIGHT-69+10), collectionViewLayout: flowLayout)
        collectionView.register(PicViewCell.self, forCellWithReuseIdentifier: "cell")
        collectionView.backgroundColor = .white
        collectionView.delegate = self
        collectionView.dataSource = self
        view.addSubview(collectionView)
        
        refresh.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        refresh.attributedTitle = NSAttributedString(string: "正在刷新")
        collectionView.addSubview(refresh)
        
        let addBtn = UIButton(frame: CGRect(x: 0, y: 70, width: 180, height: 32))
        
        addBtn.setImage(UIImage(named:"add")?.scaleImageToSize(CGSize(width: 38, height: 38)), for: .normal)
        addBtn.center.x = WIDTH/2
        view.addSubview(addBtn)
        
        addBtn.addTarget(self, action: #selector(add), for: .touchUpInside)
        
        
    }
    
    func getList(){
        
        let parameter:NSDictionary = ["SessionID":ROLogonReturmModel.sessionID,"mod":"shaitu","related":"1"]
        RONetworkMngTool.sharedNetwotkmngTool().getSharingList(parameter: parameter) { (json) in
            self.list = json
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    // @IBAction func addpic(_ sender: Any) {
    
    ////        hidesBottomBarWhenPushed = true
    ////        _ = navigationController?.pushViewController(PicAddCommitTableViewController(), animated: true)
    ////        hidesBottomBarWhenPushed = false
    //        let secondViewController = PicAddCommitTableViewController()
    //        self.present(secondViewController, animated: true, completion: nil)
    //        //self.navigationController?.pushViewController(secondViewController, animated: true)
    //    }
    @objc func add(){
        //        hidesBottomBarWhenPushed = true
        //        _ = navigationController?.pushViewController(PicAddViewController(), animated: true)
        //        hidesBottomBarWhenPushed = false
        
        self.performSegue(withIdentifier: "toadd", sender: nil)
    }
    
    
    
    @objc func refreshData(){
        getList()
        refresh.endRefreshing()
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return list?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PicViewCell
        let dict = list![indexPath.row] as! NSDictionary
        
        cell.label.text = dict.value(forKey: "title") as? String
        
        cell.imageView?.setImageWith(URL(string: "http://172.24.10.175/workout/Uploads/"+(dict.value(forKey: "thumb0") as! String)))
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let model = list?[indexPath.row]
        self.performSegue(withIdentifier: "todetail", sender: model)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toadd"{}
        else{
            let todetailViewController:PicDetailTableViewController = segue.destination as! PicDetailTableViewController
            todetailViewController.picdetail = sender as! NSDictionary
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
