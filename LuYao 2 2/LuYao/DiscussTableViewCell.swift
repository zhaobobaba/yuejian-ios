//
//  DiscussTableViewCell.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/1.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class DiscussTableViewCell: UITableViewCell {

  
    
    @IBOutlet weak var thumbb: UIImageView!
    
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var time: UILabel!
    
    @IBOutlet weak var info: UILabel!
    
    
    class func creatCell(_ tableView : UITableView) -> DiscussTableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "DiscussTableViewCell")
        if cell == nil{
            cell = (Bundle.main.loadNibNamed("DiscussTableViewCell", owner: nil, options: nil))?.first as! DiscussTableViewCell
        }
        return cell as! DiscussTableViewCell;
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

