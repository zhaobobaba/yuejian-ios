//
//  questionTableViewCell.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/1.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class questionTableViewCell: UITableViewCell {
    @IBOutlet weak var flag: UILabel!
    
    @IBOutlet weak var test: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    class func creatCell(_ tableView : UITableView) -> questionTableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "questionTableViewCell")
        if cell == nil{
            cell = (Bundle.main.loadNibNamed("questionTableViewCell", owner: nil, options: nil))?.first as! questionTableViewCell
        }
        return cell as! questionTableViewCell;
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

