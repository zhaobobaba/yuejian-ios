//
//  ContestCell.swift
//  LuYao
//
//  Created by Bobobo on 2017/10/24.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

protocol contestListDelegate {
    
}
class contestListCell:UITableViewCell{
    var contestDelegate:contestListDelegate!
    var indexPath:IndexPath!
    var Name:UILabel!
    var ContestPhoto:UIImageView!
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setView(){
        ContestPhoto = UIImageView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 200))
        self.addSubview(ContestPhoto)
        
        Name = UILabel(frame: CGRect(x: 0, y: 80, width: self.bounds.width, height: 40))
        Name.font = UIFont.systemFont(ofSize: 20)
        self.addSubview(Name)
        
        
    }
    
    override func layoutSubviews() {
        textLabel?.frame = CGRect(x: 10, y: 5, width: 120, height: 40)
        textLabel?.font = UIFont.systemFont(ofSize: 20)
    }
    
}


