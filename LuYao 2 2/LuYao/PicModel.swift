//
//  PicModel.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/1.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class PicModel: NSObject {
    var id:NSString!
    var posid:NSString!
    var itemid:NSString!
    var link:NSString!
    var listorder:NSString!
    var name:NSString!
    var thumb:NSString!
    
    class func PicModelWithDict(_ Dict:NSDictionary) -> PicModel{
        let model = PicModel()
        model.setValuesForKeys(Dict as! [String:NSObject])
        return model
    }
    
}


