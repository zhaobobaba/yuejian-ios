//
//  ClubteacherModel.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/1.
//  Copyright © 2017年 luyao. All rights reserved.
//

import Foundation
import UIKit

class ClubteacherModel: NSObject {
    
    var id:NSString!
    var name:NSString!
    var introduction:NSString!
    var thumb:NSString!
    
    class func TeacherModelWithDict(_ dict:NSDictionary)-> ClubteacherModel{
        let model = ClubteacherModel()
        
        model.setValuesForKeys(dict as! [String: NSObject])
        
        return model
        
    }
}

