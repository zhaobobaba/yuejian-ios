//
//  ROLogonViewController.swift
//  LuYao
//
//  Created by luyao on 2017/4/22.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class ROLogonViewController: UIViewController {
    
    
    @IBOutlet weak var tel: UITextField!
    
    
    
   
    
    @IBOutlet weak var password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       fetchUserNamePwd()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func logon(_ sender: UIButton) {
    
    
    
        //MBProgressHUD.showAdded(to: self.view, animated: true)
        if tel.text == "" {
            //print("用户名没有输入")
            MBProgressHUD.showDelayHUDToview(self.view, message: "用户名没有输入")
        }
        else
        {
            if password.text == ""
            {
                MBProgressHUD.showDelayHUDToview(self.view, message: "密码没有输入")
                //print("密码没有输入")
            }
            else{
                //验证用户名密码
                let parameters:NSDictionary = ["tel": tel.text!, "password": password.text!]
                RONetworkMngTool.sharedNetwotkmngTool().RONetwork_Logon(parameters, view: self.view, block: {
                    (flag) in if flag == "1"
                    {
                       
                        
              MBProgressHUD.showDelayHUDToview(self.view, message: "登录成功")
                        let _ = (ACSimpleKeychain.defaultKeychain() as AnyObject).storeUsername(self.tel.text, password: self.password.text, identifier: "user1", forService: "userpassword" )
                        print("登陆成功,转换到主界面")
                        UIApplication.shared.keyWindow!.rootViewController = RoMainTabBarViewController()
                        
                    }
                    else{
                        let _ = (ACSimpleKeychain.defaultKeychain() as AnyObject).deleteAllCredentials(forService: "userpassword")
                        let _ = (ACSimpleKeychain.defaultKeychain() as AnyObject).storeUsername("", password: "", identifier: "user1", forService: "userpassword" )
                        print("登录失败")
                    }
                })
            }
        }
        
        
    }
    func fetchUserNamePwd()
    {
        let userPwdDict = (ACSimpleKeychain.defaultKeychain() as AnyObject).credentials(forIdentifier: "user1", service: "userpassword") as NSDictionary
         if userPwdDict.count != 0 {
         tel.text = userPwdDict["username"] as? String
         password.text = userPwdDict["password"] as? String
         }
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.tel.resignFirstResponder()
        self.password.resignFirstResponder()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}


