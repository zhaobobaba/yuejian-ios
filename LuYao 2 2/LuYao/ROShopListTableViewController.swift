//
//  ROShopListTableViewController.swift
//  LuYao
//
//  Created by luyao on 2017/4/22.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class ROShopListTableViewController: UITableViewController {

    var shopList: NSArray = NSArray()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //单例 工具 方法
        RONetworkMngTool.sharedNetWorkMngTool().RONetwork_GetShopList(self.view) { (shopList: NSArray) in
            self.shopList = shopList
            self.tableView.reloadData()
        }
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  80
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.shopList.count == 0{
            return 0
        }
        return self.shopList.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "shopCell", for: indexPath)
        if self.shopList.count != 0 {
            let shop:ROShopModel = self.shopList[(indexPath as NSIndexPath).row] as! ROShopModel
            cell.textLabel!.text = shop.shopname as String
            let imageStr = shop.pic as String
            let imageUrl = URL(string: imageStr)
            cell.imageView!.setImageWith(imageUrl, placeholderImage: UIImage(named:"imageViewHolder"))
            cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        }
        // Configure the cell...
        
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentShop: ROShopModel = shopList[(indexPath as NSIndexPath).row] as! ROShopModel
        self.performSegue(withIdentifier: "toFoodList", sender: currentShop)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let foodListController: ROFoodListTableViewController = segue.destination as! ROFoodListTableViewController
        foodListController.shop = sender as! ROShopModel
    }
    
    
    //    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    //        let vc = ShopDetailTableViewController()
    //        let dict = shopList[0] as! NSDictionary
    //        vc.shopId = dict["shop_id"] as! String
    //        _ = navigationController?.pushViewController(vc, animated: true)
    //    }
    
 
    }
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    

