//
//  PicDiscussModel.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/1.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class PicDiscussModel: NSObject {
    var id:NSString!
    var shaitu_id:NSString!
    var userid:NSString!
    var reply:NSString!
    var reply_userid:NSString!
    var reply_time:NSString!
    var vstate:NSString!
    var user_name:NSString!
    var photo:NSString!
    
    class func PicDiscussModelWithDict(_ Dict:NSDictionary) -> PicDiscussModel{
        let model = PicDiscussModel()
        model.setValuesForKeys(Dict as! [String:NSObject])
        return model
    }
}


