//
//  CourseInfoDownTableViewCell.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/6.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class CourseInfoDownTableViewCell: UITableViewCell {
    
    @IBOutlet weak var coursetext: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    class func creatCell(_ tableView:UITableView)->CourseInfoDownTableViewCell{
        var cell = tableView.dequeueReusableCell(withIdentifier: "CourseInfoDownTableViewCell")
        if cell == nil{
            cell = (Bundle.main.loadNibNamed("CourseInfoDownTableViewCell", owner: nil, options: nil))?.first as! CourseInfoDownTableViewCell
        }
        return cell as! CourseInfoDownTableViewCell
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

