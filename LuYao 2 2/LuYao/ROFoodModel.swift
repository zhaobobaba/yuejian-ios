//
//  ROFoodModel.swift
//  LuYao
//
//  Created by luyao on 2017/4/22.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class ROFoodModel: NSObject {
    
    var food_id: NSNumber!
    var foodname: NSString!
    var intro: NSString!
    var pic: NSString!
    var price: NSNumber!
    var recommand: NSNumber!
    var shop_id: NSNumber!
    var type_id: NSNumber!
    
    
    class func foodModelWithDict(_ dict: NSDictionary) -> ROFoodModel {
        let model = ROFoodModel()
        //根据字典，进行模型的初始化
        model.setValuesForKeys(dict as! [String: AnyObject])
        return model
        
    }
}

