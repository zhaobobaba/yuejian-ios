//
//  CourseDetailTableViewCell.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/1.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class CourseDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var adress: UILabel!
    @IBOutlet weak var thumbb: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    class func creatCell(_ tableView:UITableView)->CourseDetailTableViewCell{
        var cell = tableView.dequeueReusableCell(withIdentifier: "CourseDetailTableViewCell")
        if cell == nil{
            cell = (Bundle.main.loadNibNamed("CourseDetailTableViewCell", owner: nil, options: nil))?.first as! CourseDetailTableViewCell
        }
        return cell as! CourseDetailTableViewCell
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

