//
//  RoMainTabBarViewController.swift
//  LuYao
//
//  Created by 路瑶 on 2017/10/10.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class RoMainTabBarViewController: RDVTabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTabBarController()
        // Do any additional setup after loading the view.
    }
    
    func setupTabBarController()
    {
        //home主页
        let HomeStoryBoard:UIStoryboard = UIStoryboard(name:"Home",bundle: nil)
        let HomeNavgationController:UIViewController = HomeStoryBoard.instantiateViewController(withIdentifier: "HomeNav")
        //let HomeViewController = UIViewController()
        //let HomeNavgationController = BaseNavigationController.init(rootViewController: HomeViewController)
        
        //BaseNavigationController.init(rootViewController: HomeViewController())
        
        //club俱乐部
        let ClubStoryBoard:UIStoryboard = UIStoryboard(name:"Club",bundle: nil)
        
        let ClubNavgationController:UIViewController = ClubStoryBoard.instantiateViewController(withIdentifier: "ClubNav")
        
        
        //晒图
        let PicStoryBoard:UIStoryboard = UIStoryboard(name:"Pic",bundle: nil)
        
        let PicNavgationController:UIViewController = PicStoryBoard.instantiateViewController(withIdentifier: "PicNav")
        
        //我的
        let MeStoryBoard:UIStoryboard = UIStoryboard(name:"Centerhome",bundle: nil)
        
        let MeNavgationController:UIViewController = MeStoryBoard.instantiateViewController(withIdentifier: "Center")
        
        
        self.viewControllers = [HomeNavgationController , ClubNavgationController,PicNavgationController, MeNavgationController]
        
        let tabBarItemImages = ["first","second","third","fifth"]
        let tabBarItemTitles = ["主页","俱乐部","晒图","培训"]
        let tabBar = self.tabBar
        tabBar?.frame = CGRect(x: (tabBar?.frame.minX)!, y: (tabBar?.frame.minY)!, width: (tabBar?.frame.width)!, height: 63)
        var index : Int = 0
        for item in self.tabBar.items as! [RDVTabBarItem]{
            let selectedimage = UIImage(named:"\(tabBarItemImages[index])"+"_select")
            let unselectedimage = UIImage(named:"\(tabBarItemImages[index])"+"_unselect")
            item.setFinishedSelectedImage(selectedimage, withFinishedUnselectedImage: unselectedimage)
            item.title = tabBarItemTitles[index]
            index += 1
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
