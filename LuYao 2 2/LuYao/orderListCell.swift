//
//  orderListCell.swift
//  LuYao
//
//  Created by luyao on 2017/6/7.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit
protocol orderListDelegate {
    func addComment(indexPath:IndexPath)
}

class orderListCell:UITableViewCell{
    var priceLabel:UILabel!
    var shopNameLabel:UILabel!
    var addressLabel:UILabel!
    private var commentBtn:UIButton!
    var indexPath:IndexPath!
    var orderDelegate:orderListDelegate!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setView()
    }
    
    func setView(){
        priceLabel = UILabel(frame: CGRect(x: 125, y: 5, width: 160, height: 40))
        priceLabel.font = UIFont.systemFont(ofSize: 20)
        self.addSubview(priceLabel)
        
        shopNameLabel = UILabel(frame: CGRect(x: 10, y: 45, width: 120, height: 40))
        shopNameLabel.font = UIFont.systemFont(ofSize: 18)
        self.addSubview(shopNameLabel)
        
        addressLabel = UILabel(frame: CGRect(x: 125, y: 45, width: 120, height: 40))
        addressLabel.font = UIFont.systemFont(ofSize: 18)
        self.addSubview(addressLabel)
        
        commentBtn = UIButton(frame: CGRect(x: UIScreen.main.bounds.width-60, y: 20, width: 50, height: 35))
        commentBtn.setTitle("评论", for: .normal)
        commentBtn.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        commentBtn.backgroundColor = UIColor.yellow
    
        commentBtn.addTarget(self, action: #selector(comment), for: .touchUpInside)
        self.addSubview(commentBtn)
        
        let line = CAShapeLayer()
        line.fillColor = UIColor.lightGray.cgColor
        line.path = UIBezierPath(rect: CGRect(x: 0, y: 79, width: UIScreen.main.bounds.width, height: 1)).cgPath
        self.layer.addSublayer(line)
    }
    
    func comment(){
        //        commentBtn.isHidden = true
        orderDelegate.addComment(indexPath: indexPath)
    }
    
    override func layoutSubviews() {
        textLabel?.frame = CGRect(x: 10, y: 5, width: 120, height: 40)
        textLabel?.font = UIFont.systemFont(ofSize: 20)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
