//
//  TeacherDetailTableViewController.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/1.
//  Copyright © 2017年 luyao. All rights reserved.
//
import UIKit
import AVKit
import AVFoundation


let width = UIScreen.main.bounds.size.width
let height = UIScreen.main.bounds.size.height
class TeacherDetailTableViewController: UITableViewController {
    
    var teacherID:NSString!
    var teachermodel:teachermodel!
    var teachervideoList = NSArray()
    var teacherQueList = NSArray()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GetteacherDetail()
        GetVideo()
        GetteacherQue()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var basecell = UITableViewCell()
        if indexPath.section == 0{
            let cell = HeaderIntroTableViewCell.creatCell(tableView)
            if teachermodel != nil{
                let imageStr = "http://172.24.10.175/workout/Uploads/" + (teachermodel.thumb! as String)
                let imageUrl = URL(string: imageStr)
                let data = try?Data(contentsOf: imageUrl!)
                if data != nil{
                    let image = UIImage(data: data!)
                    cell.thumbb.image = image}
                else{
                    cell.thumbb.image = UIImage(named: "GoLogin.jpg")
                }
                
                cell.username.text = teachermodel.realname! as String
                let str = "\(teachermodel.province_name!)" + "\(teachermodel.city_name!)"
                cell.adress.text = str
                cell.common.text = teachermodel.actcate_name as String
                cell.intro.text = teachermodel.introduction! as String
                basecell = cell
            }}
        else if indexPath.section == 1{
            if teachervideoList.count != 0{
                let cell:TeacherVideoTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TeacherVideoTableViewCell", for: indexPath) as! TeacherVideoTableViewCell
                cell.TT.text = "大师带你练"
                cell.number = teachervideoList.count
                for i in 0..<teachervideoList.count{
                    let model = teachervideoList[i] as! teachervideomodel
                    let imageStr = "http://172.24.10.175/workout/Uploads/" + (model.thumb! as String)
                    let imageUrl = URL(string: imageStr)
                    let data = try! Data(contentsOf: imageUrl!)
                    let image = UIImage(data: data)
                    (cell.imageViews[i] as! SingleView).pic.image = image
                    (cell.imageViews[i] as! SingleView).Tittle.text = model.mvideo_name! as String
                    (cell.imageViews[i] as! SingleView).Btn.tag = i
                    (cell.imageViews[i] as! SingleView).Btn.addTarget(self, action: #selector(PlayVideo), for: .touchUpInside)
                    basecell = cell
                }}
                
            else{
                let cell = EXVideoTableViewCell.creatCell(tableView)
                
                basecell = cell
            }
        }
        else{
            let model = teacherQueList[indexPath.row] as! teacherquestionmodel
            let cell = questionTableViewCell.creatCell(tableView)
            
            cell.test.text = model.question as String
            if model.state == "1"{
                cell.flag.text = "|已解决"
                cell.flag.textColor = UIColor.green
            }else{
                cell.flag.text = "|未回复"
                cell.flag.textColor = UIColor.blue
            }
            basecell = cell
        }
        basecell.selectionStyle = UITableViewCellSelectionStyle.none
        return basecell
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = teacherQueList[indexPath.row] as! teacherquestionmodel
        self.performSegue(withIdentifier: "toanswer", sender: model)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let teacherController:TeacherAnswerTableViewController = segue.destination as! TeacherAnswerTableViewController
        teacherController.teacher = sender as! teacherquestionmodel
    }
    
    
    
    func GetteacherDetail(){
        let para = ["mod":"master","id":teacherID] as NSDictionary
        RONetworkMngTool.sharedNetwotkmngTool().teacherDetail(para,view:self.view){(teachermodel) in
            self.teachermodel = teachermodel as! teachermodel
            self.tableView.reloadData()
        }
        
    }
    func GetteacherQue(){
        let para = ["mod":"masterfaq","master_id":teacherID,"SessionID":ROLogonReturmModel.sessionID,"page":1,"listorder":"state"] as NSDictionary
        
        RONetworkMngTool.sharedNetwotkmngTool().Network_GetListView(para, view: self.view){(teacherQueList) in
            self.teacherQueList = teacherQueList
            
            self.tableView.reloadData()
            
        }
    }
    
    func GetVideo(){
        let para = ["mod":"master","SessionID":ROLogonReturmModel.sessionID,"related":"1","id":teacherID,"map":"mvideo"] as NSDictionary
        RONetworkMngTool.sharedNetwotkmngTool().Network_Getrelated(para, view:self.view){(teachervideoList) in
            
            print(teachervideoList)
            self.teachervideoList = teachervideoList
            self.tableView.reloadData()
        }
    }
    func PlayVideo(sender:UIButton){

        print("play")
                let model = teachervideoList[sender.tag] as! teachervideomodel
                print("路径：http://172.24.10.175/workout/Uploads/tvideo/video/\(model.url!)")
                let str = "http://172.24.10.175/workout/Uploads/tvideo/video/" + "\(model.url!)"

                let videoURL = URL(string: str)!
                //定义一个视频播放器，通过本地文件路径初始化
                let player = AVPlayer(url: videoURL)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.present(playerViewController, animated: true) {

                    playerViewController.player!.play()




    }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }
    
override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 2{
            return teacherQueList.count
        }
        return 1
    }
    
    
    
override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 2{
            return height * 0.05
        }
        return height * 0.2
    
    }
    //创建左边约束

 
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
    
}

