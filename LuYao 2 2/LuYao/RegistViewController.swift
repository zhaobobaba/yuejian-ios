//
//  RegistViewController.swift
//  LuYao
//
//  Created by luyao on 2017/9/29.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {
    
   
    
    @IBOutlet weak var tel: UITextField!
    
    
    
    @IBOutlet weak var password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func register(_ sender: Any) {
        
        
        
        //        MBProgressHUD.showAdded(to: self.view, animated: true)
        if tel.text == "" {
            //print("用户名没有输入")
            MBProgressHUD.showDelayHUDToview(self.view, message: "联系电话没有输入")
        }
        else
        {
            if password.text == ""
            {
                MBProgressHUD.showDelayHUDToview(self.view, message: "密码没有输入")
                //print("密码没有输入")
                
            }
            else{
                
                let parameters:NSDictionary = ["tel": tel.text!, "password": password.text!]
                //print(parameters)
                RONetworkMngTool.sharedNetwotkmngTool().RONetwork_register(parameters, view: self.view, block: {
                    (flag) in if flag == "1"
                    {
                        MBProgressHUD.showDelayHUDToview(self.view, message: "注册成功")
                        let _ = self.navigationController?.popViewController(animated: true)
                        print("注册成功")}
                    else{
                        MBProgressHUD.hide(for: self.view, animated: true)
                        print("注册失败")
                    }
                })
                
            }
            
            
            
        }
        
    }
    @IBAction func Canc(_ sender: UIButton){
        
        
        let _ = self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

