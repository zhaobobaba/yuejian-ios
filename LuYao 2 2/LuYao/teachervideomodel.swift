//
//  teachervideomodel.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/1.
//  Copyright © 2017年 luyao. All rights reserved.
//

import Foundation
import UIKit

class teachervideomodel: NSObject {
    var id:NSString!
    var mvideo_name:NSString!
    var thumb:NSString!
    var degree:NSString!
    var hits:NSString!
    var url:NSString!
    
    
    class func teachervideomodelWithDict(_ Dict:NSDictionary) -> teachervideomodel{
        let model = teachervideomodel()
        model.setValuesForKeys(Dict as! [String:NSObject])
        return model
    }
    
}

