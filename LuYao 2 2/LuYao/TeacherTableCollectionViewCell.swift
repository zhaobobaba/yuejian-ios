//
//  TeacherTableCollectionViewCell.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/1.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class TeacherTableCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var pic: UIImageView!
    
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var clubname: UILabel!
    @IBOutlet weak var text: UILabel!
    @IBOutlet weak var teachertec: UILabel!
    @IBOutlet weak var teachername: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}

