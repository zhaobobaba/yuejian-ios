//
//  HeaderIntroTableViewCell.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/1.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class HeaderIntroTableViewCell: UITableViewCell {
    
    @IBOutlet weak var thumbb: UIImageView!
    
    @IBOutlet weak var common: UILabel!
    @IBOutlet weak var username: UILabel!
    
    @IBOutlet weak var intro: UILabel!
    @IBOutlet weak var adress: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    class func creatCell(_ tableView : UITableView) -> HeaderIntroTableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "HeaderIntroTableViewCell")
        if cell == nil{
            cell = (Bundle.main.loadNibNamed("HeaderIntroTableViewCell", owner: nil, options: nil))?.first as! HeaderIntroTableViewCell
        }
        return cell as! HeaderIntroTableViewCell;
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

