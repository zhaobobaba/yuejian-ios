//
//  PicViewCell.swift
//  LuYao
//
//  Created by 路瑶 on 2017/10/20.
//  Copyright © 2017年 luyao. All rights reserved.
//

import Foundation
class PicViewCell: UICollectionViewCell {
    var label:UILabel!
    var imageView:UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        viewConfig()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func viewConfig(){
        imageView = UIImageView(frame: bounds)
        contentView.addSubview(imageView)
        
        label = UILabel(frame: CGRect(x: 10, y: 20, width: bounds.width-15, height: 20))
        label.font = UIFont.boldSystemFont(ofSize: 20)
        contentView.addSubview(label)
    }
}

