//
//  ClubTableViewCell.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/7.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class ClubTableViewCell: UITableViewCell {
    var club:ClubModel!
    @IBOutlet weak var clubname: UILabel!
    @IBOutlet weak var thumbb: UIImageView!
    @IBOutlet weak var districtname: UILabel!
    
    
    class func creatCell(_ tableView:UITableView)->ClubTableViewCell{
        var cell = tableView.dequeueReusableCell(withIdentifier: "ClubTableViewCell")
        if cell == nil{
            cell = (Bundle.main.loadNibNamed("ClubTableViewCell", owner: nil, options: nil))?.first as! ClubTableViewCell
        }
        return cell as! ClubTableViewCell
    }//连接一下下
    //    var club_name:NSString!{
    //        willSet(newValue){
    //            clubname.text = newValue as String;
    //        }
    //    }
    //    var district_name:NSString!{
    //        willSet(newValue){
    //            districtname.text = newValue as String;
    //        }
    //    }
    //    var thumb:NSString!{
    //        willSet(newValue){
    //            thumbb = newValue as String;
    //        }
    //    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
