//
//  teacherquestionmodel.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/1.
//  Copyright © 2017年 luyao. All rights reserved.
//

import Foundation

class teacherquestionmodel: NSObject {
    var id:NSString!
    var master_id:NSString!
    var user_id:NSString!
    var question:NSString!
    var question_time:NSString!
    var answer:NSString!
    var answer_time:NSString!
    var state:NSString!
    
    class func teacherquestionmodelWithDict(_ Dict:NSDictionary) -> teacherquestionmodel{
        let model = teacherquestionmodel()
        model.setValuesForKeys(Dict as! [String:NSObject])
        return model
    }
}

