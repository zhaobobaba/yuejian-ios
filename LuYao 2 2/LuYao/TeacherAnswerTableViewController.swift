//
//  TeacherAnswerTableViewController.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/1.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class TeacherAnswerTableViewController: UITableViewController {
    
    
    var teacher:teacherquestionmodel!
    var foottextfield:UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if teacher.state == "1"{
            CreatFootView()
        }
        
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var basecell = UITableViewCell()
        if indexPath.section == 0{
            
            let cell = DetailQuestionUserAnswerTableViewCell.creatCell(tableView)
            cell.testt.text = teacher.question as String
            cell.timee.text = teacher.question_time as String
            basecell = cell
        }
        else{
            
            
            let cell = DetailQuestionTeacherAnswerTableViewCell.creatCell(tableView)
            cell.testt.text = teacher.answer as String
            cell.timee.text = teacher.answer_time as String
            basecell = cell
        }
        basecell.selectionStyle = UITableViewCellSelectionStyle.none
        return basecell
    }
    
    
    func CreatFootView(){
        let footerView:UIView = UIView(frame: CGRect(x:0, y: 0, width: width, height: 60))
        foottextfield = UITextField(frame: CGRect(x: 20, y: 20, width: width*4/5 - 20, height: 60))
        let footButton:UIButton = UIButton(frame: CGRect(x:  width*4/5 + 5, y: 20, width: width/5 - 20, height: 40))
        foottextfield.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
        foottextfield.placeholder = "请输入您想说的话～"
        footButton.setTitle("回复", for: .normal)
        footButton.setTitleColor(UIColor.white, for: .normal)
        footButton.backgroundColor = UIColor.orange
        footButton.addTarget(self, action: #selector(comment), for: .touchUpInside)
        footerView.addSubview(footButton)
        footerView.addSubview(foottextfield)
        tableView.tableFooterView = footerView
        
    }
    
    func comment(){
        let para = ["mod":"masterfaq","SessionID":ROLogonReturmModel.sessionID,"Id":teacher.id,"answer":self.foottextfield.text!,"state":0] as NSDictionary
        RONetworkMngTool.sharedNetwotkmngTool().Network_Update(para, view: self.view)
        {(res) in
            
        }
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        if teacher.state == "0"
        {return 2}
        else{
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }
    
    
    
}

