//
//  YScrollView.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/9.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class YScrollView: UIScrollView,UIScrollViewDelegate{
    let WIDTH = UIScreen.main.bounds.width
    let HEIGHT = UIScreen.main.bounds.height
    private var timer:Timer!
    private var pageControl:UIPageControl!
    
    private var firstAction:((String)->Void)?
    private var secondAction:((String)->Void)?
    private var thirdAction:((String)->Void)?
    
    var ids:Array<String>!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    func initialize(images:[String]){
        contentSize = CGSize(width: WIDTH*3, height: bounds.height)
        setContentOffset(CGPoint(x:WIDTH,y:0), animated: true)
        showsHorizontalScrollIndicator = false
        showsVerticalScrollIndicator = false
        isPagingEnabled = true
        delegate = self
        
        for index in 0...2{
            let imageView = UIImageView(frame: CGRect(x: CGFloat(index)*WIDTH, y: 0, width: WIDTH, height: bounds.height))
            imageView.tag = index+1
            imageView.isUserInteractionEnabled = true
            imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onClick)))
            addSubview(imageView)
            if index == images.count{
                imageView.image = UIImage(named: "placeholder")
            }else{
                let imgData = NSData(contentsOf: URL(string: images[index])!)
                imageView.image = UIImage(data: imgData! as Data)
            }
        }
        
        pageControl = UIPageControl(frame: CGRect(x: WIDTH/2-25, y: bounds.height-24+frame.origin.y, width: 50, height: 30))
        pageControl.numberOfPages = 3
        pageControl.currentPage = 0
        pageControl.pageIndicatorTintColor = UIColor.gray
        pageControl.currentPageIndicatorTintColor = UIColor.white
        pageControl.isUserInteractionEnabled = false
        
        timerInit()
    }
    
    func imageGesture(firstAction:((String)->Void)?,secondAction:((String)->Void)?,thirdAction:((String)->Void)?){
        self.firstAction = firstAction
        self.secondAction = secondAction
        self.thirdAction = thirdAction
    }
    
    @objc private func onClick(gesture:UITapGestureRecognizer){
        switch gesture.view!.tag {
        case 1:
            if ids.count >= 1{
                firstAction!(ids[0])
            }
        case 2:
            if ids.count >= 2{
                secondAction!(ids[1])
            }
        case 3:
            if ids.count >= 3{
                thirdAction!(ids[2])
            }
        default:
            break
        }
    }
    
    func pageControlInit(view:UIView){
        view.addSubview(pageControl)
    }
    
    private func timerInit(){
        timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(timerChanged), userInfo: nil, repeats: true)
    }
    
    @objc private func timerChanged(){
        setContentOffset(CGPoint(x: WIDTH*2, y: 0), animated: true)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let offset = contentOffset.x
        
        let firstImg = viewWithTag(2)!
        let secondImg = viewWithTag(3)!
        let thirdImg = viewWithTag(1)!
        
        var currentpage = pageControl.currentPage
        
        if offset > WIDTH{
            if currentpage == 2{
                currentpage = 0
            }
            else if currentpage >= 0 && currentpage < 3{
                currentpage += 1
            }
            if secondImg.frame.origin.x >= WIDTH{
                secondImg.frame.origin.x -= WIDTH
            }
            else{
                secondImg.frame.origin.x += WIDTH*2
            }
            if firstImg.frame.origin.x >= WIDTH{
                firstImg.frame.origin.x -= WIDTH
            }
            else{
                firstImg.frame.origin.x += WIDTH*2
            }
            if thirdImg.frame.origin.x >= WIDTH{
                thirdImg.frame.origin.x -= WIDTH
            }
            else{
                thirdImg.frame.origin.x += WIDTH*2
            }
        }
        if offset < WIDTH{
            if currentpage == 0{
                currentpage = 3
            }
            else if currentpage > 0 && currentpage < 3{
                currentpage -= 1
            }
            if secondImg.frame.origin.x <= WIDTH{
                secondImg.frame.origin.x += WIDTH
            }
            else{
                secondImg.frame.origin.x -= WIDTH*2
            }
            if firstImg.frame.origin.x <= WIDTH{
                firstImg.frame.origin.x += WIDTH
            }
            else{
                firstImg.frame.origin.x -= WIDTH*2
            }
            if thirdImg.frame.origin.x <= WIDTH{
                thirdImg.frame.origin.x += WIDTH
            }
            else{
                thirdImg.frame.origin.x -= WIDTH*2
            }
        }
        setContentOffset(CGPoint(x:WIDTH,y:0), animated: false)
        pageControl.currentPage = currentpage
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        let firstImg = viewWithTag(2)!
        let secondImg = viewWithTag(3)!
        let thirdImg = viewWithTag(1)!
        setContentOffset(CGPoint(x: WIDTH, y: 0), animated: false)
        var currentpage = pageControl.currentPage
        if currentpage == 0{
            currentpage += 1
            firstImg.frame.origin.x = 0
            secondImg.frame.origin.x = WIDTH
            thirdImg.frame.origin.x = WIDTH*2
        }
        else if currentpage == 1{
            currentpage += 1
            secondImg.frame.origin.x = 0
            thirdImg.frame.origin.x = WIDTH
            firstImg.frame.origin.x = WIDTH*2
        }
        else{
            currentpage = 0
            thirdImg.frame.origin.x = 0
            firstImg.frame.origin.x = WIDTH
            secondImg.frame.origin.x = WIDTH*2
        }
        pageControl.currentPage = currentpage
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        timer.invalidate()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        timerInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}


