//
//  MasterDetailViewController.swift
//  LuYao
//
//  Created by Bobobo on 2017/10/24.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit
class MasterDetailViewController: UIViewController {
    var infoDict:NSDictionary!
    var videoPlayer:VideoPlayer!
    var hideState = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewConfig()
    }
    
    func viewConfig(){
        view.backgroundColor = UIColor.white
        
        //        let navBar = CustomNavBar(frame: CGRect(origin: .zero, size: CGSize(width: WIDTH, height: 64)))
        //        navBar.initialize(text: nil)
        //        view.addSubview(navBar)
        
        let scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height-64))
        //scrollView.contentInset = .never
        scrollView.contentSize = CGSize(width: 0, height: 364+(UIScreen.main.bounds.height-69)/3)
        scrollView.backgroundColor = view.backgroundColor
        scrollView.showsVerticalScrollIndicator = false
        view.addSubview(scrollView)
        
        let imageView = UIImageView()
        let pURL = "http://172.24.10.175/workout/Uploads/"
        imageView.setImageWith(URL(string: pURL+(infoDict.value(forKey: "thumb") as! String)),
                               placeholderImage: UIImage(named:"placeholder"))
        imageView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: (UIScreen.main.bounds.height-69)/3)
        scrollView.addSubview(imageView)
        
        let label = UILabel(frame: CGRect(x: 15, y: imageView.bounds.height+74, width: UIScreen.main.bounds.width-30, height: 20))
        label.text = "视频简介"
        label.textAlignment = .center
        label.layer.cornerRadius = 5
        label.layer.borderColor = UIColor.lightGray.cgColor
        //label.layer.borderWidth = 1
        view.addSubview(label)
        
        let joinBtn = UIButton(frame: CGRect(x: 0, y: label.frame.origin.y+210, width: 150, height: 44))
        joinBtn.backgroundColor = UIColor.red
        joinBtn.center.x = UIScreen.main.bounds.width/2
        joinBtn.setTitle("关注", for: .normal)
        joinBtn.layer.cornerRadius = 8
        joinBtn.adjustsImageWhenDisabled = false
        view.addSubview(joinBtn)
        
        videoPlayer = VideoPlayer(frame: CGRect(x: 0, y: label.frame.origin.y+50, width: UIScreen.main.bounds.width, height: 150))
        videoPlayer.initialize(itemUrl:RO_Video)
        view.addSubview(videoPlayer)
        
        joinBtn.addTarget(self, action: #selector(add), for: .touchUpInside)
        //
        //        navBar.backBtn.addTarget(self, action: #selector(back), for: .touchUpInside)
        
        videoPlayer.fullBtn.addTarget(self, action: #selector(fullScreen), for: .touchUpInside)
    }
    
    @objc func add(sender:UIButton){
        sender.isEnabled = false
        let alert = UIAlertController(title: "是否关注", message: nil, preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: "确认", style: .default) { (_) in
            let parameter:NSDictionary = [
                "SessionID"     :   LoginModel.sessionID,
                "mvideo_id"     :   self.infoDict.value(forKey: "id") as! String,
                "user_id"       :   LoginModel.userID
            ]
            RONetworkMngTool.shared.focusMaster(parameter: parameter, block: {
                DispatchQueue.main.async {
                    let popup = Popup()
                    popup.appear(title: "关注成功", action: {
                        self.back()
                    })
                }
            })
        }
        let cancelAction = UIAlertAction(title: "取消", style: .default, handler: nil)
        alert.addAction(confirmAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    
    @objc func fullScreen(){
        let pw = videoPlayer.bounds.width
        let ph = videoPlayer.bounds.height
        if !hideState{
            UIView.animate(withDuration: 0.3, animations: {
                self.videoPlayer.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi/180)*CGFloat(90)).concatenating(CGAffineTransform(scaleX: UIScreen.main.bounds.width/ph, y: UIScreen.main.bounds.height/pw)).concatenating(CGAffineTransform(translationX: 0, y: -25.833-64))
            })
            hideState = !hideState
            setNeedsStatusBarAppearanceUpdate()
        }
        else{
            UIView.animate(withDuration: 0.3, animations: {
                self.videoPlayer.transform = .identity
            })
            hideState = !hideState
            setNeedsStatusBarAppearanceUpdate()
        }
    }
    
    override var prefersStatusBarHidden: Bool{
        return hideState
    }
    
    @objc func back(){
        videoPlayer.disAppear()
        _ = navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}



