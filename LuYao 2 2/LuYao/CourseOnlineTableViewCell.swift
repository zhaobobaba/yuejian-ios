//
//  CourseOnlineTableViewCell.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/1.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class CourseOnlineTableViewCell: UITableViewCell {
    
    @IBOutlet weak var d: UIButton!
    @IBOutlet weak var c: UIButton!
    @IBOutlet weak var b: UIButton!
    @IBOutlet weak var a: UIButton!
    @IBOutlet weak var hitss: UILabel!
    @IBOutlet weak var typenam: UILabel!
    @IBOutlet weak var clubnam: UILabel!
    @IBOutlet weak var thumbb: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    class func creatCell(_ tableView:UITableView)->CourseOnlineTableViewCell{
        var cell = tableView.dequeueReusableCell(withIdentifier: "CourseOnlineTableViewCell")
        if cell == nil{
            cell = (Bundle.main.loadNibNamed("CourseOnlineTableViewCell", owner: nil, options: nil))?.first as! CourseOnlineTableViewCell
        }
        return cell as! CourseOnlineTableViewCell
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

