//
//  Exten.swift
//  LuYao
//
//  Created by 路瑶 on 2017/10/20.
//  Copyright © 2017年 luyao. All rights reserved.
//


import Foundation

import UIKit
import Photos

extension UIImage{
    func scaleImageToSize(_ size:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(size,false,UIScreen.main.scale)
        self.draw(in: CGRect(x: 0,y: 0, width: size.width, height: size.height))
        let reSizeImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return reSizeImage
    }
}

