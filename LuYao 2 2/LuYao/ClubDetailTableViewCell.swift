//
//  ClubDetailTableViewCell.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/7.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class ClubDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var pic: UIImageView!
    
    @IBOutlet weak var info: UILabel!
    
    @IBOutlet weak var movie: UIButton!
    @IBOutlet weak var course: UIButton!
    // @IBOutlet weak var Information: UIButton!
    
    class func creatCell(_ tableView:UITableView)->ClubDetailTableViewCell{
        var cell = tableView.dequeueReusableCell(withIdentifier: "ClubDetailTableViewCell")
        if cell == nil{
            cell = (Bundle.main.loadNibNamed("ClubDetailTableViewCell", owner: nil, options: nil))?.first as! ClubDetailTableViewCell
        }
        return cell as! ClubDetailTableViewCell
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

