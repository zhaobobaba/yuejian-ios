//
//  teachermodel.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/1.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class teachermodel: NSObject {
    var id:NSString!
    var realname:NSString!
    var thumb:NSString!
    var thumbsize:NSString!
    var province_id:NSString!
    var province_name:NSString!
    var city_id:NSString!
    var city_name:NSString!
    var actcate_id:NSString!
    var actcate_name:NSString!
    var introduction:NSString!
    var club_id:NSString!
    var club_name:NSString!
    var video:NSString!
    var userid:NSString!
    var hits:NSString!
    var insert_time:NSString!
    var listorder:NSString!
    var vstate:NSString!
    
    class func teachermodelWithDict(_ dict:NSDictionary) -> teachermodel{
        let model = teachermodel()
        model.setValuesForKeys(dict as! [String: NSObject])
        
        
        return model
        
    }
}


