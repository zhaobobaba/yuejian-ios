//
//  ROshopTableViewCell.swift
//  LuYao
//
//  Created by luyao on 2017/4/22.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class ROshopTableViewCell: UITableViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //cell布局，只要cell大小发生变化就会调用
    override func layoutSubviews() {
        super.layoutSubviews()
        //固定imageview的大小和位置
        self.imageView!.frame = CGRect(x: 8, y: 4, width: 100, height: 72)
        //设定textlabel的位置
        var tmpframe = self.textLabel!.frame
        tmpframe.origin.x = 116
        self.textLabel!.frame = tmpframe
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
