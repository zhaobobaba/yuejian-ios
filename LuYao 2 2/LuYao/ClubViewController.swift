//
//  ClubViewController.swift
//  LuYao
//
//  Created by enon on 2017/10/12.
//  Copyright © 2017年 luyao. All rights reserved.
//
import UIKit

class ClubViewController: UIViewController,UITableViewDelegate,UITableViewDataSource  {
    @IBOutlet weak var tableview: UITableView!
    
    lazy var refresh = UIRefreshControl()
    
    var clubsList: NSArray = NSArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        refresh.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        refresh.attributedTitle = NSAttributedString(string: "正在刷新")
        tableview.addSubview(refresh)
        self.tableview.delegate = self
        self.tableview.dataSource = self
        getClubList()
        // Do any additional setup after loading the view.
    }
    
    @objc func refreshData(){
        getClubList()
        refresh.endRefreshing()
    }
    func getClubList()
    {
        
        RONetworkMngTool.sharedNetwotkmngTool().RONetwork_ClubList(self.view){
            (clubList) -> Void in
            print(clubList)
            self.clubsList = clubList
            self.tableview.reloadData()
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var basecell:UITableViewCell = UITableViewCell()
        
        let cell = ClubTableViewCell.creatCell(tableview)
        let currentclub:ClubModel = self.clubsList[(indexPath as NSIndexPath).row] as! ClubModel
        cell.clubname.text = (currentclub.club_name as NSString) as String
        cell.districtname.text = (currentclub.district_name as NSString) as String
        
        let imageStr = "http://172.24.10.175/workout/Uploads/" + (currentclub.thumb! as String)
        let imageUrl = URL(string: imageStr)
        let data = try! Data(contentsOf: imageUrl!)
        let image = UIImage(data: data)
        
        cell.thumbb.image =  image
        
        basecell = cell
        return basecell
        
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.clubsList.count
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt: IndexPath) -> CGFloat
    {
        
        return 150
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = ClubTableViewCell.creatCell(tableView)
        let currentclub:ClubModel = clubsList[(indexPath as NSIndexPath).row] as! ClubModel
        //let dic:NSDictionary = ["id":currentclub.id]
        self.performSegue(withIdentifier: "ToClubDetail", sender:currentclub)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let ClubDetailController:ClubDetailTableViewController = segue.destination  as! ClubDetailTableViewController
        ClubDetailController.club = sender as! ClubModel
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
