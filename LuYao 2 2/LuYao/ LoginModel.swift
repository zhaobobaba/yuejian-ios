//
//   LoginModel.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/7.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class LoginModel: NSObject {
    static var sessionID:String!
    static var username:String!
    static var userID:String!
    static var userTel:String!
    
    class func getUserInfo(dict:NSDictionary){
        LoginModel.sessionID = dict.value(forKey: "sessionid") as! String
        LoginModel.username = dict.value(forKey: "user_name") as! String
        LoginModel.userID = dict.value(forKey: "id") as! String
        LoginModel.userTel = dict.value(forKey: "tel") as! String
        print(LoginModel.sessionID)
    }
}

