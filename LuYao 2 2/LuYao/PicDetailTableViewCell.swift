//
//  PicDetailTableViewCell.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/1.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class PicDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var thumbb: UIImageView!
    
    @IBOutlet weak var userimage: UIImageView!
    
    @IBOutlet weak var ontime: UILabel!
    @IBOutlet weak var username: UILabel!
    
    class func creatCell(_ tableView : UITableView) -> PicDetailTableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "PicDetailTableViewCell")
        if cell == nil{
            cell = (Bundle.main.loadNibNamed("PicDetailTableViewCell", owner: nil, options: nil))?.first as! PicDetailTableViewCell
        }
        return cell as! PicDetailTableViewCell;
    }
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

