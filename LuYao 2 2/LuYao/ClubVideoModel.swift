//
//  ClubVideoModel.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/1.
//  Copyright © 2017年 luyao. All rights reserved.
//

import Foundation
class ClubVideoModel: NSObject {
    var id:NSString!
    var name:NSString!
    var thumb:NSString!
    var videopath:NSString!
    
    class func VideoModelWithDict(_ dict:NSDictionary)-> ClubVideoModel{
        let model = ClubVideoModel()
        
        model.setValuesForKeys(dict as! [String: NSObject])
        /*
         model.id = dict.value(forKey: "id") as! NSString!
         model.name = dict.value(forKey: "name") as! NSString!
         model.thumb = dict.value(forKey: "thumb") as! NSString!
         model.videopath = dict.value(forKey: "videopath") as! NSString!
         */
        return model
        
    }
    
    
}
