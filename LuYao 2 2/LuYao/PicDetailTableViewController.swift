//
//  PicDetailTableViewController.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/1.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class PicDetailTableViewController: UITableViewController {
    
    
    var picdetail:NSDictionary!
    var foottextfield:UITextField!
    var list = NSArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "晒图详情"
        Getdiscuss()
        CreatFootView()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var basecell = UITableViewCell()
        
        if indexPath.section == 0{
            
            let cell = PicDetailTableViewCell.creatCell(tableView)
            let name = picdetail["shaier"] as? NSDictionary
            cell.username.text = name?["user_name"] as? String
            cell.ontime.text = picdetail["update_time"] as? String
            if picdetail["thumb0"] != nil{
                let imageStr = "http://172.24.10.175/workout/Uploads/" + (picdetail["thumb0"]! as! String)
                let imageUrl = URL(string: imageStr)
                let data = try?Data(contentsOf: imageUrl!)
                if data != nil{
                    let image = UIImage(data: data!)
                    cell.thumbb.image = image
                }
                
                
            }
            basecell = cell
        }
        else{
            if list.count != 0
            {
                let model = list[indexPath.row] as! PicDiscussModel
                let cell = DiscussTableViewCell.creatCell(tableView)
                cell.username.text = model.user_name as String
                if model.reply != nil{
                    cell.info.text = model.reply as String}
                else
                {
                    cell.info.text = "Info"
                }
                cell.time.text = model.reply_time as String
                
                if model.photo != nil{
                    let imageStr = "http://172.24.10.175/workout/Uploads/" + (model.photo as String)
                    let imageUrl = URL(string: imageStr)
                    let data = try?Data(contentsOf: imageUrl!)
                    if data != nil{
                        let image = UIImage(data: data!)
                        cell.thumbb.image = image
                    }
                    
                }
                basecell = cell
            }
        }
        
        
        basecell.selectionStyle = UITableViewCellSelectionStyle.none
        return basecell
    }
    
    func Getdiscuss(){
        let parameter = ["mod":"shaireply","SessionID":ROLogonReturmModel.sessionID,"related":1,"shaitu_id":picdetail["id"] ]as NSDictionary
        RONetworkMngTool.sharedNetwotkmngTool().Network_GetSListView(parameter, view: self.view){(discusslist) in
            self.list = discusslist
            self.tableView.reloadData()
        }
    }
    func adddiscuss(){
        let a =  picdetail["id"]
        let para = ["mod":"shaireply","SessionID":ROLogonReturmModel.sessionID,"shaitu_id":picdetail["id"],"userid":ROLogonReturmModel.userid,"reply_userid":ROLogonReturmModel.userid,"reply":foottextfield.text!] as NSDictionary
        RONetworkMngTool.sharedNetwotkmngTool().Network_Create(para, view: self.view){(res) in
            if res == "1"{
                self.Getdiscuss()
            }
        }
        
    }
    
    
    func CreatFootView(){
        let footerView:UIView = UIView(frame: CGRect(x:0, y: 0, width: width, height: 60))
        foottextfield = UITextField(frame: CGRect(x: 20, y: 10, width: width*4/5 - 20, height: 40))
        foottextfield.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
        foottextfield.placeholder = "请输入您想评论的内容【约健】"
        let footButton:UIButton = UIButton(frame: CGRect(x:  width*4/5 + 5, y: 10, width: width/5 - 10, height: 40))
        footButton.setTitle("回复", for: .normal)
        footButton.setTitleColor(UIColor.white, for: .normal)
        footButton.backgroundColor = UIColor.orange
        footButton.addTarget(self, action: #selector(adddiscuss), for: .touchUpInside)
        footerView.addSubview(footButton)
        footerView.addSubview(foottextfield)
        tableView.tableFooterView = footerView
        
    }
    
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return height * 0.43
        }else{
            
            return height * 0.13
            
        }}
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0{
            return 1
        }else if section == 1 && list.count != 0{
            return list.count
        }else{
            return 1
        }
    }
    
}

