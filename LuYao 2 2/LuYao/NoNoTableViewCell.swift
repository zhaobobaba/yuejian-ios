//
//  NoNoTableViewCell.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/7.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class NoNoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var bear: UIImageView!
    @IBOutlet weak var infooo: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    class func creatCell(_ tableView:UITableView)->NoNoTableViewCell{
        var cell = tableView.dequeueReusableCell(withIdentifier: "NoNoTableViewCell")
        if cell == nil{
            cell = (Bundle.main.loadNibNamed("NoNoTableViewCell", owner: nil, options: nil))?.first as! NoNoTableViewCell
        }
        return cell as! NoNoTableViewCell
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

