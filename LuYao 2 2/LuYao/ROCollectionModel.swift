//
//  ROCollectionModel.swift
//  LuYao
//
//  Created by luyao on 2017/5/27.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class ROCollectionModel: NSObject {
    var address: NSString!
    var collect_id: NSNumber!
    var flag : NSNumber!
    var food_id: NSNumber!
    var foodname: NSString!
    var pic: NSString!
    var price: NSNumber!
    var shop_id: NSNumber!
    var shopname: NSString!
    var user_id: NSNumber!
    var username: NSString!
    
    class func collectionModelWithDict(_ dict: NSDictionary ) -> ROCollectionModel {
        let model = ROCollectionModel()
        //根据字典，进行模型的初始化
        model.setValuesForKeys(dict as! [String : AnyObject])
        return model
    }
    
}
