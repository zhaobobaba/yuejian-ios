//
//  CourseTableViewCell.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/7.
//  Copyright © 2017年 luyao. All rights reserved.
//
import UIKit

class CourseTableViewCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var thumbb: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    class func creatCell(_ tableView:UITableView)->CourseTableViewCell{
        var cell = tableView.dequeueReusableCell(withIdentifier: "CourseTableViewCell")
        if cell == nil{
            cell = (Bundle.main.loadNibNamed("CourseTableViewCell", owner: nil, options: nil))?.first as! CourseTableViewCell
        }
        return cell as! CourseTableViewCell
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

