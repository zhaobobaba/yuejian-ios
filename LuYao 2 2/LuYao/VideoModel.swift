//
//  VideoModel.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/1.
//  Copyright © 2017年 luyao. All rights reserved.
//

import Foundation
class VideoModel: NSObject {
    var id:NSString!
    var tvideo_name:NSString!
    var thumb:NSString!
    var url:NSString!
    
    class func VideoModelWithDict(_ dict:NSDictionary)-> VideoModel{
        let model = VideoModel()
        
        model.setValuesForKeys(dict as! [String: NSObject])
        /*
         model.id = dict.value(forKey: "id") as! NSString!
         model.tvideo_name = dict.value(forKey: "tvideo_name") as! NSString!
         model.thumb = dict.value(forKey: "thumb") as! NSString!
         model.url = dict.value(forKey: "url") as! NSString!
         */
        return model
        
    }
}

