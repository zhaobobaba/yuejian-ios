//
//  OnlineCourseTableViewController.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/1.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class OnlineCourseTableViewController: UITableViewController {
    
    var Course:CourseUpModel!
    var turebarbtnItem:UIBarButtonItem!
    var rightBtn:UIButton = UIButton.init()
    var videosList = NSArray()
    var array = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        automaticallyAdjustsScrollViewInsets = false
        self.title = Course.train_name as String
        rightBtn.sizeToFit()
        rightBtn.addTarget(self, action: #selector(Ifcollect), for: UIControlEvents.touchUpInside)
        turebarbtnItem = UIBarButtonItem.init(customView: rightBtn)
        self.navigationItem.rightBarButtonItem = turebarbtnItem
        self.rightBtn.setImage(UIImage.init(named: "buzai.png"), for: .normal)
        CreatFootView()
        getvideo()
        
    }
    /*
     override func viewWillAppear(_ animated: Bool) {
     getvideo()
     }*/
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var basecell = UITableViewCell()
        if indexPath.section == 0{
            let cell = CourseDetailTableViewCell.creatCell(tableView)
            let imageStr = "http://172.24.10.175/workout/Uploads/" + (Course.thumb! as String)
            let imageUrl = URL(string: imageStr)
            let data = try! Data(contentsOf: imageUrl!)
            let image = UIImage(data: data)
            cell.thumbb.image = image
            cell.adress.text = Course.scene_name as String
            basecell = cell
        }
        else if indexPath.section == 1{
            if videosList.count != 0{
                let cell:CourseVideosTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CourseVideosTableViewCell", for: indexPath) as! CourseVideosTableViewCell

                cell.number = videosList.count
                cell.TT.text = "相关视频"
                for i in 0..<videosList.count{
                    let model = videosList[i] as! VideoModel
                    let imageStr = "http://172.24.10.175/workout/Uploads/" + (model.thumb! as String)
                    let imageUrl = URL(string: imageStr)
                    let data = try! Data(contentsOf: imageUrl!)
                    let image = UIImage(data: data)
                    (cell.imageViews[i] as! SingleView).pic.image = image

                    (cell.imageViews[i] as! SingleView).Btn.tag = i
                    (cell.imageViews[i] as! SingleView).Btn.addTarget(self, action: #selector(PlayVideo), for: .touchUpInside)
                }
                basecell = cell
            }
            else{
                let cell = EXVideoTableViewCell.creatCell(tableView)

                basecell = cell


            }
        }
        else if indexPath.section == 2{
            let cell = CourseInfoDownTableViewCell.creatCell(tableView)

            cell.coursetext.text = Course.introduction as String
            basecell = cell
        }

        basecell.selectionStyle = UITableViewCellSelectionStyle.none
        return basecell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return height * 0.3
        }else if indexPath.section == 1{
            return height * 0.2
        }else if indexPath.section == 2{
            return height * 0.2
        }
        else{
            return height * 0.1
        }
        
    }
    
    func Ifcollect(){
        let para = ["mod":"trainfavor","SessionID":ROLogonReturmModel.sessionID,"train_id":1,"user_id":ROLogonReturmModel.userid] as NSDictionary
        RONetworkMngTool.sharedNetwotkmngTool().Network_Create(para, view: self.view){(res) in
            if res == "1"{
                
                print("关注成功")
                
            }
        }
        
        
    }
    func getvideo(){
        let parameters = ["mod":"training","SessionID":ROLogonReturmModel.sessionID,"id":Course.id,"map":"tvideo","related":1] as NSDictionary
        RONetworkMngTool.sharedNetwotkmngTool().Network_Gettvideorelated(parameters, view: self.view){(videoList) in
            self.videosList = videoList
            self.tableView.reloadData()
        }
        
    }
    
    func PlayVideo(sender:UIButton){
        let model = videosList[sender.tag] as! VideoModel
        print("路径：http://172.24.10.175/workout/Uploads/tvideo/video/\(model.url!)")
        let str = "http://172.24.10.175/workout/Uploads/tvideo/video/" + "\(model.url!)"
        
        let videoURL = URL(string: str)!
        
        //定义一个视频播放器，通过本地文件路径初始化
        let player = AVPlayer(url: videoURL)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        
        
        self.addChildViewController(playerViewController)
        self.view.addSubview(playerViewController.view)
        playerViewController.view.frame = self.view.frame

        player.play()

        
//        self.present(playerViewController, animated: true) {
//            self.present(playerViewController, animated: true, completion: {
//                playerViewController.player!.play()
//            })
//
//
//
//      }
        
    }
    
    
    func CreatFootView(){
        let footerView:UIView = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 60))
        let footButton:UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: width, height: 60))
        footButton.setTitle("下载课程", for: .normal)
        footButton.setTitleColor(UIColor.white, for: .normal)
        footButton.backgroundColor = UIColor.orange
        footButton.addTarget(self, action: #selector(XiaZai), for: .touchUpInside)
        footerView.addSubview(footButton)
        tableView.tableFooterView = footerView
        
    }
    func XiaZai(){
        
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        return 10
        
    }
    
    
    
    
}
