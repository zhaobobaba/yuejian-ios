//
//  ClubcourseModel.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/1.
//  Copyright © 2017年 luyao. All rights reserved.
//

import Foundation
import UIKit

class ClubcourseModel: NSObject {
    var id:NSString!
    var course_name:NSString!
    var course_price:NSString!
    var introduction:NSString!
    var notice:NSString!
    var bmnotice:NSString!
    var start_date:NSString!
    var end_date:NSString!
    var thumb:NSString!
    var degree:NSString!
    
    class func CourseModelWithDict(_ dict:NSDictionary)-> ClubcourseModel{
        let model = ClubcourseModel()
        
        model.setValuesForKeys(dict as! [String: NSObject])
        
        return model
        
    }
    
    
}
