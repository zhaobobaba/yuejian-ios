//
//  CourseUpModel.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/1.
//  Copyright © 2017年 luyao. All rights reserved.
//

import Foundation
class CourseUpModel: NSObject {
    var id:NSString!
    var train_name:NSString!
    var thumb:NSString!
    var thumbsize:NSString!
    var introduction:NSString!
    var train_type:NSString!
    var scene_id:NSString!
    var scene_name:NSString!
    var body_name:NSString!
    var degree:NSString!
    var userid:NSString!
    var hits:NSString!
    var insert_time:NSNull!
    var update_time:NSString!
    var check_userid:NSString!
    var checktime:NSNull!
    var checkstate:NSString!
    var listorder:NSString!
    var vstate:NSString!
    
    class func CourseUpModelWithDict(_ dict:NSDictionary)-> CourseUpModel{
        let model = CourseUpModel()
        model.id = dict.value(forKey: "id") as! NSString!
        model.train_name = dict.value(forKey: "train_name") as! NSString!
        model.thumb = dict.value(forKey: "thumb") as! NSString!
        model.thumbsize = dict.value(forKey: "thumbsize") as! NSString!
        model.introduction = dict.value(forKey: "introduction") as! NSString!
        model.scene_id = dict.value(forKey: "scene_id") as! NSString!
        model.scene_name = dict.value(forKey: "scene_name") as! NSString!
        model.hits = dict.value(forKey: "hits") as! NSString!
        model.body_name = dict.value(forKey: "body_name") as! NSString!
        model.degree = dict.value(forKey: "degree") as! NSString!
        model.userid = dict.value(forKey: "userid") as! NSString!
        model.insert_time = dict.value(forKey: "insert_time") as! NSNull!
        model.update_time = dict.value(forKey: "update_time") as! NSString!
        model.check_userid = dict.value(forKey: "check_userid") as! NSString!
        model.checktime = dict.value(forKey: "checktime") as! NSNull!
        model.checkstate = dict.value(forKey: "checkstate") as! NSString!
        model.listorder = dict.value(forKey: "listorder") as! NSString!
        model.vstate = dict.value(forKey: "vstate") as! NSString!
        model.train_type = dict.value(forKey: "train_type") as! NSString!
        
        
        
        return model
    }
    
    
    
}

