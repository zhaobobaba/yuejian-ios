//
//  ClubdetailModel.swift
//  LuYao
//
//  Created by 路瑶 on 2017/10/16.
//  Copyright © 2017年 luyao. All rights reserved.
//

import Foundation
import UIKit

class ClubdetailModel: NSObject {
    var id:NSString!
    var club_name:NSString!
    var province_id:NSString!
    var province_name:NSString!
    var city_id:NSString!
    var city_name:NSString!
    var district_id:NSString!
    var district_name:NSString!
    var longitude:NSString!
    var latitude:NSString!
    var introduction:NSString!
    var equipsuggest:NSString!
    var thumb:NSString!
    var open_time:NSString!
    var club_tel:NSString!
    var club_contact:NSString!
    var comment_num:NSString!
    var member_num:NSString!
    var fans_num:NSString!
    var userid:NSString!
    var hits:NSString!
    var insert_time:NSString!
    var listorder:NSString!
    var vstate:NSString!
    
    class func ClubdetailModelWithDict(_ dict:NSDictionary)-> ClubdetailModel{
        let model = ClubdetailModel()
        
        model.id = dict.value(forKey: "id") as! NSString!
        model.club_name = dict.value(forKey: "club_name") as! NSString!
        model.province_id = dict.value(forKey: "province_id") as! NSString!
        model.province_name = dict.value(forKey: "province_name") as! NSString!
        model.city_id = dict.value(forKey: "city_id") as! NSString!
        model.city_name = dict.value(forKey: "city_name") as! NSString!
        model.district_id = dict.value(forKey: "district_id") as! NSString!
        model.district_name = dict.value(forKey: "district_name") as! NSString!
        model.longitude = dict.value(forKey: "longitude") as! NSString!
        model.latitude = dict.value(forKey: "latitude") as! NSString!
        model.introduction = dict.value(forKey: "introduction") as! NSString!
        model.equipsuggest = dict.value(forKey: "equipsuggest") as! NSString!
        model.thumb = dict.value(forKey: "thumb") as! NSString!
        model.open_time = dict.value(forKey: "open_time") as! NSString!
        model.club_tel = dict.value(forKey: "club_tel") as! NSString!
        model.club_contact = dict.value(forKey: "club_contact") as! NSString!
        model.comment_num = dict.value(forKey: "comment_num") as! NSString!
        model.member_num = dict.value(forKey: "member_num") as! NSString!
        model.fans_num = dict.value(forKey: "fans_num") as! NSString!
        model.userid = dict.value(forKey: "userid") as! NSString!
        model.hits = dict.value(forKey: "hits") as! NSString!
        model.insert_time = dict.value(forKey: "insert_time") as! NSString!
        model.listorder = dict.value(forKey: "listorder") as! NSString!
        model.vstate = dict.value(forKey: "vstate") as! NSString!
        
        
        return model
    }
}
