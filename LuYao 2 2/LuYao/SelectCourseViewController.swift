//
//  SelectCourseViewController.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/1.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class SelectCourseViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    lazy var refresh = UIRefreshControl()
    var courseolineList = NSArray()
    var courseofflineList = NSArray()
    
    var List:NSArray!
    
    
    @IBAction func change(_ sender: UISegmentedControl) {
        
        switch segmentt.selectedSegmentIndex {
        case 0:
            GetCourseonline()
            
        case 1:
            GetCourseoffline()
        default:
            break;
        }
        
    }
    
    @IBOutlet weak var CourseView: UITableView!
    @IBOutlet weak var segmentt: UISegmentedControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "课程列表"
        
        refresh.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        refresh.attributedTitle = NSAttributedString(string: "正在刷新")
        CourseView.addSubview(refresh)
        
        self.CourseView.delegate = self
        self.CourseView.dataSource = self
        segmentt.selectedSegmentIndex = 0
        GetCourseonline()
        //GetCourseoffline()
        // Do any additional setup after loading the view.
    }
    
    
    @objc func refreshData(){
        GetCourseonline()
        GetCourseoffline()
        refresh.endRefreshing()
    }
    
    //获取线上课程列表
    func  GetCourseonline(){
        let para = ["mod":"training"] as NSDictionary
        RONetworkMngTool.sharedNetwotkmngTool().Network_GetCourseonlineListView(para, view: self.view) { (courseolineList:NSArray) in
            
            self.courseolineList = courseolineList
            self.CourseView.reloadData()
            
        }
    }
    //获取线下课程列表
    func  GetCourseoffline(){
        let para = ["mod":"train"] as NSDictionary
        RONetworkMngTool.sharedNetwotkmngTool().Network_GetCourseofflineListView(para, view: self.view) { (courseofflineList :NSArray) in
            
            self.courseofflineList = courseofflineList
            print(courseofflineList)
            self.List = courseofflineList
            self.CourseView.reloadData()
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if segmentt.selectedSegmentIndex == 0{
            if courseolineList.count != 0{
                
                return courseolineList.count
                
            }
            return 0
        }
        else if segmentt.selectedSegmentIndex == 1{
            if section == 0{
                return 1
            }else{
                if courseofflineList.count != 0{
                    
                    return courseofflineList.count
                }
                return 0
            }
        }else {
            return 0
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var basecell = UITableViewCell()
        if segmentt.selectedSegmentIndex == 0{
            let model:CourseUpModel = courseolineList[indexPath.row] as! CourseUpModel
            
            let cell = CourseOnlineTableViewCell.creatCell(tableView)
            
            cell.clubnam.text = "club"
            let Stri = "点击量:" + "\(model.hits!)"
            cell.hitss.text = Stri
            var type:String!
            if model.degree == "1"{
                type = "入门"
            }else if model.degree == "2"{
                type = "菜鸟"
            }else if model.degree == "3"{
                type = "进阶"
            }
            cell.typenam.text = type
            
            let imageStr = "http://172.24.10.175/workout/Uploads/" + (model.thumb! as String)
            let imageUrl = URL(string: imageStr)
            let data = try! Data(contentsOf: imageUrl!)
            let image = UIImage(data: data)
            cell.thumbb.image = image
            basecell = cell
            
            
        }
        else if segmentt.selectedSegmentIndex == 1{
            
            
            if indexPath.section == 0{
                
                let cell = XibButtonTableViewCell.creatCell(tableView)
                cell.all.tag = 0
                cell.all.addTarget(self, action: #selector(display), for: .touchUpInside)
                cell.rm.tag = 1
                cell.rm.addTarget(self, action: #selector(display), for: .touchUpInside)
                cell.cn.tag = 2
                cell.cn.addTarget(self, action: #selector(display), for: .touchUpInside)
                cell.jt.tag = 3
                cell.jt.addTarget(self, action: #selector(display), for: .touchUpInside)
                basecell = cell
            }else{
                
                
                let model:CourseModel = courseofflineList[indexPath.row] as! CourseModel
                let cell = CourseOnlineTableViewCell.creatCell(tableView)
                cell.clubnam.text = "Offlinecourse"
                let Stri = "点击量:" + "\(model.hits!)"
                cell.hitss.text = Stri
                var type:String!
                if model.degree == "1"{
                    type = "入门"
                }else if model.degree == "2"{
                    type = "菜鸟"
                }else if model.degree == "3"{
                    type = "进阶"
                }
                cell.typenam.text = type
                print(model.id)
                
                let imageStr = "http://172.24.10.175/workout/Uploads/" + (model.thumb! as String)
                let imageUrl = URL(string: imageStr)
                let data = try! Data(contentsOf: imageUrl!)
                let image = UIImage(data: data)
                cell.thumbb.image = image
                
                basecell = cell
            }
            
        }
        basecell.selectionStyle = UITableViewCellSelectionStyle.none
        return basecell
    }
    
    
    func display(sender:UIButton){
        
        var object:CourseModel!
        if sender.tag == 0{
            GetCourseoffline()
        }else if sender.tag == 1{
            let selectList:NSMutableArray = NSMutableArray()
            for i in 0 ..< List.count {
                object = List[i] as! CourseModel
                if object.degree == "1"{
                    selectList.add(object)
                }
                self.courseofflineList = selectList
                self.CourseView.reloadData()
                
            }
        }else if sender.tag == 2{
            let selectList:NSMutableArray = NSMutableArray()
            for i in 0 ..< List.count {
                object = List[i] as! CourseModel
                if object.degree == "2"{
                    selectList.add(object)
                }
                self.courseofflineList = selectList
                self.CourseView.reloadData()
            }
        }
        else if sender.tag == 3{
            let selectList:NSMutableArray = NSMutableArray()
            for i in 0 ..< List.count {
                object = List[i] as! CourseModel
                if object.degree == "3"{
                    selectList.add(object)
                }
                self.courseofflineList = selectList
                self.CourseView.reloadData()
            }
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if segmentt.selectedSegmentIndex == 0{
            let currentcourse:CourseUpModel = courseolineList[(indexPath as NSIndexPath).row] as! CourseUpModel
            self.performSegue(withIdentifier: "tocourseonlinedetail", sender: currentcourse)
        }
        else if segmentt.selectedSegmentIndex == 1{
            let currentcourse:CourseModel = courseofflineList[(indexPath as NSIndexPath).row] as! CourseModel
            self.performSegue(withIdentifier: "tocourseofflinedetail", sender: currentcourse)
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "tocourseonlinedetail"{
            let CourseDetailController:OnlineCourseTableViewController = segue.destination as! OnlineCourseTableViewController
            CourseDetailController.Course = sender as! CourseUpModel
            
        }
        else
            if segue.identifier == "tocourseofflinedetail"{
                let CourseDetailController:OfflineCourseTableViewController = segue.destination as! OfflineCourseTableViewController
                CourseDetailController.Course = sender as! CourseModel
                
                
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if segmentt.selectedSegmentIndex == 0{
            return UIScreen.main.bounds.height/2
        }
        else if segmentt.selectedSegmentIndex == 1{
            if indexPath.section == 0{
                return height * 0.05
            }
            else{
                return UIScreen.main.bounds.height/2
            }
        }
        
        
        return UIScreen.main.bounds.height/2
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if segmentt.selectedSegmentIndex == 0{
            return 1
        }
        else if segmentt.selectedSegmentIndex == 1{
            return 2
        }else{
            return 0
        }
    }
    
    
    
}
