//
//  RONetworkMngTool.swift
//  LuYao
//
//  Created by luyao on 2017/9/29.
//  Copyright © 2017年 luyao. All rights reserved.
//

import Foundation
var SessionID:String!
var userId: NSString!
let single = RONetworkMngTool()

class RONetworkMngTool: NSObject {
  
    static var shared = RONetworkMngTool()
    class func sharedNetwotkmngTool() -> RONetworkMngTool
    {
        
        return single
    }
    
    func RONetwork_register(_ params: NSDictionary, view:UIView , block:@escaping (_ flag: NSString) -> Void)  {
        MBProgressHUD.showAdded(to: view,animated: true)
        let Manager = AFHTTPRequestOperationManager()
        Manager.post("http://172.24.10.175/workout/api.php/reg",
                     parameters: params,
                     success: { (operation: AFHTTPRequestOperation?,
                        responseObject: Any?) in
                        MBProgressHUD.hide(for: view, animated: true)
                        //print("zhuce")
                        //print(responseObject)
                        let RegisterReturnModel = RORegisterReturmModel.RegisterModelWithDict(responseObject as! NSDictionary)
                        
                        if RegisterReturnModel.id != nil{
                            print("注册成功")
                            block("1")
                        } else{
                            print("注册失败")
                            block("0")
                        }
                        
                        
        },
                     failure: { (operation: AFHTTPRequestOperation?,
                        error: Error?) in
                        MBProgressHUD.hide(for: view,animated: true)
                        MBProgressHUD.showDelayHUDToview(view, message: "网络加载失败")
                        block("0")
                        
        })
        
    }
    
    func RONetwork_Logon(_ parameters: NSDictionary, view:UIView , block:@escaping (_ flag: NSString) -> Void)  {
        //逃逸闭包，函数结束以后会调用
        MBProgressHUD.showAdded(to: view,animated: true)
        let afManager = AFHTTPRequestOperationManager()
        afManager.get("http://172.24.10.175/workout/api.php/login",
                      parameters: parameters, success: {(operation:AFHTTPRequestOperation?, responseObject:Any?) in
                        print("用户登录信息介绍")
                        print(responseObject)
                        /*let userid = (responseObject as! NSDictionary).object(forKey: "userid") as! NSString
                         if userid != "0"{
                         print("登录成功")
                         }
                         else{
                         print("登录失败")
                         }*/
                        MBProgressHUD.hide(for: view, animated: true)
                        let logonReturnModel = ROLogonReturmModel.LogonReturnModelWithDict(responseObject as! NSDictionary)
                        if logonReturnModel.id != "0" {
                            LoginModel.getUserInfo(dict: responseObject as! NSDictionary)
                            //userId = logonReturnModel.userid
                            print("登陆成功")
                            block("1")
                        } else {
                            MBProgressHUD.showDelayHUDToview(view, message: logonReturnModel.id as String)
                            //print("登录失败")
                            block("0")
                        }
                        
        }, failure: {(operation:AFHTTPRequestOperation?, error:Error?) in
            MBProgressHUD.hide(for: view,animated: true)
            MBProgressHUD.showDelayHUDToview(view, message: "网络加载失败")
            block("0")
        })
    }
    
    func RONetwork_ClubList(_ view:UIView, block: @escaping(_ courseList:NSArray)-> Void){
        let afManager = AFHTTPRequestOperationManager()
        
        afManager.get("http://172.24.10.175/workout/api.php/lists/mod/club", parameters: nil, success: { (Operation:AFHTTPRequestOperation?,responseObject:Any?) in
            
            let clubArray = responseObject as! NSArray
            let clubMutableArray:NSMutableArray = NSMutableArray()
            for clubDict in clubArray{
                let object = clubDict as! NSDictionary
                let club:ClubModel = ClubModel.ClubModelWithDict(object)
                clubMutableArray.add(club)
                
            }
            block(clubMutableArray)
            
            
        }, failure: { (Operation:AFHTTPRequestOperation?,error:Error?) in
            MBProgressHUD.hide(for: view, animated: true)
        })
    }
    func RONetwork_ClubCourseList(_ parameters: NSDictionary, view:UIView, block: @escaping(_ courseList:NSArray)-> Void){
        let afManager = AFHTTPRequestOperationManager()
        afManager.get("http://172.24.10.175/workout/api.php/get/mod/club/SessionID/2ul9sj8vlfofj122e1s98048k2/related/1/id/1/map/Clubcourse", parameters: nil, success: { (Operation:AFHTTPRequestOperation?,responseObject:Any?) in
            
            let clubArray = responseObject as! NSArray
            let clubMutableArray:NSMutableArray = NSMutableArray()
            for clubDict in clubArray{
                let object = clubDict as! NSDictionary
                let club:ClubModel = ClubModel.ClubModelWithDict(object)
                clubMutableArray.add(club)
                
            }
            block(clubMutableArray)
            
            
        }, failure: { (Operation:AFHTTPRequestOperation?,error:Error?) in
            MBProgressHUD.hide(for: view, animated: true)
        })
    }
    func RONetwork_homeupdetail(_ view:UIView, block: @escaping(_ homeList:NSArray)-> Void){
        let afManager = AFHTTPRequestOperationManager()
        
        afManager.post("http://172.24.10.175/workout/api.php/lists/mod/acmrecom", parameters: nil, success: { (Operation:AFHTTPRequestOperation?,responseObject:Any?) in
            print(responseObject)
            
//            let homeArray = responseObject as! NSArray
//            let homeMutableArray:NSMutableArray = NSMutableArray()
//            for homeDict in homeArray{
//                let object = homeDict as! NSDictionary
//                let home:HomeTableModel = HomeTableModel.HomeTableModelWithDict(object)
//                homeMutableArray.add(home)
//
//            }
//            block(homeMutableArray)
            
        }, failure: { (Operation:AFHTTPRequestOperation?,error:Error?) in
            MBProgressHUD.hide(for: view, animated: true)
        })
    }
    
//    func RONetwork_hometodetail(_ parameters: NSDictionary, view:UIView, block: @escaping(_ todetailList:HomeDetailModel)-> Void){
//        let afManager = AFHTTPRequestOperationManager()
//        print(parameters)
//        afManager.post("http://172.24.10.175/workout/api.php/get", parameters: parameters, success: { (Operation:AFHTTPRequestOperation?,responseObject:Any?) in
//            print("detail")
//            print(responseObject)
//
//
//            let homedetailArray = responseObject as! NSDictionary
//
//            let homedetail:HomeDetailModel = HomeDetailModel.HomeDetailModelhDict(homedetailArray)
//
//            block(homedetail)
//
    
//            
//        }, failure: { (Operation:AFHTTPRequestOperation?,error:Error?) in
//            // print("111111")
//            MBProgressHUD.hide(for: view, animated: true)
//        })
//    }
//    
    
    func focusMaster(parameter:NSDictionary,block:@escaping ()->Void){
        post(urlStr: RO_FocusMaster, parameter: parameter, block: nil)
        block()
    }
    
    func RONetwork_clubdetail(_ parameters: NSDictionary, view:UIView , block:@escaping (_ clubdetail: ClubdetailModel) -> Void)  {
        //逃逸闭包，函数结束以后会调用
        //MBProgressHUD.showAdded(to: view,animated: true)
        let afManager = AFHTTPRequestOperationManager()
        afManager.get("http://172.24.10.175/workout/api.php/get/mod/club",
                      parameters: parameters, success: {(operation:AFHTTPRequestOperation?, responseObject:Any?) in
                        print("俱乐部详细信息")
                        print(responseObject)
                        
                        
                        let clubdetailArray = responseObject as! NSDictionary
                        let object:ClubdetailModel = ClubdetailModel.ClubdetailModelWithDict(clubdetailArray)
                        
                        block(object)
                        
                        
        }, failure: {(operation:AFHTTPRequestOperation?, error:Error?) in
            MBProgressHUD.hide(for: view,animated: true)
            //MBProgressHUD.showDelayHUDToview(view, message: "网络加载失败")
        })
    }
    
    
    func getSharingList(parameter:NSDictionary,block:@escaping(NSArray)->Void){
        let afManager = AFHTTPRequestOperationManager()
        afManager.post("http://172.24.10.175/workout/api.php/lists", parameters: parameter, success: { (datatask, json) in
            let array = json as! NSArray
            block(array)
        }) { (datatask, error) in
            //self.popup.appear(title: "网络异常", action: nil)
        }
    }
    
    func getclubteacherselect(_ parameters:NSDictionary, view:UIView, block: @escaping(_ List:NSArray)-> Void){
        let afManager = AFHTTPRequestOperationManager()
        afManager.post("http://172.24.10.175/workout/api.php/get", parameters: parameters, success: {(operation:AFHTTPRequestOperation?, responseObject:Any?)  in
            
            //            let clubdetailArray = responseObject as! NSDictionary
            //            let object:ClubteacherModel = ClubteacherModel.TeacherModelWithDict(clubdetailArray)
            
            let homedetailArray = responseObject as! NSArray
            let homedetailMutableArray:NSMutableArray = NSMutableArray()
            for homedetailDict in homedetailArray{
                let object = homedetailDict as! NSDictionary
                let homedetail:ClubteacherModel = ClubteacherModel.TeacherModelWithDict(object)
                homedetailMutableArray.add(homedetail)
                
            }
            block(homedetailMutableArray)
            
            
            
        }, failure: {(operation:AFHTTPRequestOperation?, error:Error?) in
            MBProgressHUD.hide(for: view,animated: true)
            //MBProgressHUD.showDelayHUDToview(view, message: "网络加载失败")
        })
    }
    
    func Network_ShaiTu(_ parameter:NSDictionary,image:UIImage,block:@escaping ()->Void){
        let afManager =   AFHTTPSessionManager()
        afManager.post("http://172.24.10.175/workout/api.php/fcreate", parameters: parameter, constructingBodyWith: { (fromData) in
            let data = UIImageJPEGRepresentation(image, 0.1)
            fromData?.appendPart(withFileData: data!, name: "thumb0", fileName: "image.jpeg", mimeType: "image/jpeg")
        }, success:{ (task, json) in
            block()
        }
            , failure: { (task, json) in
                block()
        }
        )
        
        
        
    }
    
    
    
    func getclubcourseselect(_ parameters:NSDictionary, view:UIView, block: @escaping(_ List:NSArray)-> Void){
        let afManager = AFHTTPRequestOperationManager()
        afManager.post("http://172.24.10.175/workout/api.php/get", parameters: parameters, success: {(operation:AFHTTPRequestOperation?, responseObject:Any?)  in
            
            let a = responseObject as! NSDictionary
            let homedetailArray = a["Clubcourse"] as! NSArray
            
            let homedetailMutableArray:NSMutableArray = NSMutableArray()
            for homedetailDict in homedetailArray{
                let object = homedetailDict as! NSDictionary
                let homedetail:ClubcourseModel = ClubcourseModel.CourseModelWithDict(object)
                homedetailMutableArray.add(homedetail)
                
            }
            block(homedetailMutableArray)
            
        }, failure: {(operation:AFHTTPRequestOperation?, error:Error?) in
            MBProgressHUD.hide(for: view,animated: true)
            //MBProgressHUD.showDelayHUDToview(view, message: "网络加载失败")
        })
    }
    
    
    
    
    func teacherDetail(_ parameters:NSDictionary, view:UIView,  block:@escaping(_ DaShi:NSObject)-> Void){
        let afManager = AFHTTPRequestOperationManager()
        afManager.post("http://172.24.10.175/workout/api.php/get", parameters: parameters, success: {(operation:AFHTTPRequestOperation?, responseObject:Any?)  in
            //let res = json as! NSDictionary
            let homedetailArray = responseObject as! NSDictionary
            
            let homedetail:teachermodel = teachermodel.teachermodelWithDict(homedetailArray)
            
            block(homedetail)
            
        }, failure: {(operation:AFHTTPRequestOperation?, error:Error?) in
            MBProgressHUD.hide(for: view,animated: true)
            //MBProgressHUD.showDelayHUDToview(view, message: "网络加载失败")
        })
    }
    func Network_GetteacherListView(_ parameters:NSDictionary, view:UIView, block: @escaping(_ courseList:NSArray)-> Void){
        let afManager = AFHTTPRequestOperationManager()
        afManager.get("http://172.24.10.175/workout/api.php/lists", parameters: parameters,  success: { (datatask, json) in
            
            let homedetailArray = json as!NSArray
            let homedetailMutableArray:NSMutableArray = NSMutableArray()
            
            for homedetailDict in homedetailArray{
                let object = homedetailDict as! NSDictionary
                let homedetail:teachermodel = teachermodel.teachermodelWithDict(object)
                homedetailMutableArray.add(homedetail)
                
            }
            block(homedetailMutableArray)
            
            
        }) { (datatask, json) in
            MBProgressHUD.hide(for: view, animated: true)
        }}
    func Network_GetactiveListView(_ parameters:NSDictionary, view:UIView, block: @escaping(_ courseList:NSArray)-> Void){
        let afManager = AFHTTPRequestOperationManager()
        afManager.get("http://172.24.10.175/workout/api.php/lists", parameters: parameters,   success: { (datatask, json) in
            
            let homedetailArray = json as!NSArray
            let homedetailMutableArray:NSMutableArray = NSMutableArray()
            for homedetailDict in homedetailArray{
                let object = homedetailDict as! NSDictionary
                
                let homedetail:ActiveModel = ActiveModel.ActiveModelWithDict(object)
                homedetailMutableArray.add(homedetail)
                
            }
            block(homedetailMutableArray)
            
        }, failure: {(operation:AFHTTPRequestOperation?, error:Error?) in
            MBProgressHUD.hide(for: view,animated: true)
            //MBProgressHUD.showDelayHUDToview(view, message: "网络加载失败")
        })
    }
    
    
    func getclubvideoselect(_ parameters:NSDictionary, view:UIView, block: @escaping(_ List:NSArray)-> Void){
        let afManager = AFHTTPRequestOperationManager()
        afManager.post("http://172.24.10.175/workout/api.php/get", parameters: parameters, success: {(operation:AFHTTPRequestOperation?, responseObject:Any?)  in
            
            let homedetail = responseObject as! NSDictionary
            let homedetailArray = homedetail["Clubvideo"] as!NSArray
            let homedetailMutableArray:NSMutableArray = NSMutableArray()
            
            for homedetailDict in homedetailArray{
                let object = homedetailDict as! NSDictionary
                let homedetail:VideoModel = VideoModel.VideoModelWithDict(object)
                homedetailMutableArray.add(homedetail)
                
            }
            block(homedetailMutableArray)
            
            
            
        }, failure: {(operation:AFHTTPRequestOperation?, error:Error?) in
            MBProgressHUD.hide(for: view,animated: true)
            //MBProgressHUD.showDelayHUDToview(view, message: "网络加载失败")
        })
    }
    func Network_Getrelated(_ parameters:NSDictionary, view:UIView, block: @escaping(_ List:NSArray)-> Void){
        let afManager = AFHTTPRequestOperationManager()
        afManager.post("http://172.24.10.175/workout/api.php/get", parameters: parameters,  success: {(operation:AFHTTPRequestOperation?, responseObject:Any?)  in
            
            let a = responseObject as! NSDictionary
            let homedetailArray = a["mvideo"] as! NSArray
            
            let homedetailMutableArray:NSMutableArray = NSMutableArray()
            for homedetailDict in homedetailArray{
                let object = homedetailDict as! NSDictionary
                
                let homedetail:teachervideomodel = teachervideomodel.teachervideomodelWithDict(object)
                homedetailMutableArray.add(homedetail)
                
            }
            block(homedetailMutableArray)
            
        }, failure: {(operation:AFHTTPRequestOperation?, error:Error?) in
            MBProgressHUD.hide(for: view,animated: true)
            //MBProgressHUD.showDelayHUDToview(view, message: "网络加载失败")
        })
    }
    func Network_GetCourseonlineListView(_ parameters:NSDictionary, view:UIView, block: @escaping(_ courseList:NSArray)-> Void){
        let afManager = AFHTTPRequestOperationManager()
        afManager.get("http://172.24.10.175/workout/api.php/lists", parameters: parameters,  success: { (datatask, json) in
            
            
            let homedetailArray = json as!NSArray
            let homedetailMutableArray:NSMutableArray = NSMutableArray()
            
            for homedetailDict in homedetailArray{
                let object = homedetailDict as! NSDictionary
                
                let homedetail:CourseUpModel = CourseUpModel.CourseUpModelWithDict(object)
                homedetailMutableArray.add(homedetail)
                
            }
            block(homedetailMutableArray)
            
            
        }) { (datatask, json) in
            MBProgressHUD.hide(for: view, animated: true)
        }}
    
    
    func Network_GetCourseofflineListView(_ parameters:NSDictionary, view:UIView, block: @escaping(_ courseList:NSArray)-> Void){
        let afManager = AFHTTPRequestOperationManager()
        afManager.get("http://172.24.10.175/workout/api.php/lists", parameters: parameters,  success: { (datatask, json) in
            
            
            let homedetailArray = json as!NSArray
            let homedetailMutableArray:NSMutableArray = NSMutableArray()
            
            for homedetailDict in homedetailArray{
                let object = homedetailDict as! NSDictionary
                let homedetail:CourseModel = CourseModel.CourseModelWithDict(object)
                homedetailMutableArray.add(homedetail)
                
            }
            block(homedetailMutableArray)
            
            
            
        }) { (datatask, json) in
            MBProgressHUD.hide(for: view, animated: true)
        }}
    
    
    func Network_GetListView(_ parameters:NSDictionary, view:UIView, block: @escaping(_ courseList:NSArray)-> Void){
        let afManager = AFHTTPRequestOperationManager()
        afManager.get("http://172.24.10.175/workout/api.php/lists", parameters: parameters,  success: { (datatask, json) in
            
            
            let homedetailArray = json as!NSArray
            let homedetailMutableArray:NSMutableArray = NSMutableArray()
            
            for homedetailDict in homedetailArray{
                let object = homedetailDict as! NSDictionary
                let homedetail:teacherquestionmodel = teacherquestionmodel.teacherquestionmodelWithDict(object)
                homedetailMutableArray.add(homedetail)
                
            }
            block(homedetailMutableArray)
            
            
        }) { (datatask, json) in
            MBProgressHUD.hide(for: view, animated: true)
        }}
    
    func Network_GetSListView(_ parameters:NSDictionary, view:UIView,  block: @escaping(_ courseList:NSArray)-> Void){
        let afManager = AFHTTPRequestOperationManager()
        afManager.get("http://172.24.10.175/workout/api.php/lists", parameters: parameters, success: { (datatask, json) in
            
            let homedetailArray = json as!NSArray
            let homedetailMutableArray:NSMutableArray = NSMutableArray()
            
            for homedetailDict in homedetailArray{
                let object = homedetailDict as! NSDictionary
                let homedetail:PicDiscussModel = PicDiscussModel.PicDiscussModelWithDict(object)
                homedetailMutableArray.add(homedetail)
                
            }
            block(homedetailMutableArray)
            
            
        }){ (datatask, error) in
            MBProgressHUD.hide(for: view, animated: true)
        }}
    
    func Network_Gettvideorelated(_ parameters:NSDictionary, view:UIView,block: @escaping(_ List:NSArray)-> Void){
        let afManager = AFHTTPRequestOperationManager()
        afManager.post("http://172.24.10.175/workout/api.php/get", parameters: parameters, success: {(operation:AFHTTPRequestOperation?, responseObject:Any?)  in
            print(responseObject)
            let homedetail = responseObject as! NSDictionary
            let homedetailArray = homedetail["tvideo"] as!NSArray
            let homedetailMutableArray:NSMutableArray = NSMutableArray()
            
            for homedetailDict in homedetailArray{
                let object = homedetailDict as! NSDictionary
                let homedetail:VideoModel = VideoModel.VideoModelWithDict(object)
                homedetailMutableArray.add(homedetail)
                
            }
            block(homedetailMutableArray)
            
        }, failure: {(operation:AFHTTPRequestOperation?, error:Error?) in
            MBProgressHUD.hide(for: view,animated: true)
            //MBProgressHUD.showDelayHUDToview(view, message: "网络加载失败")
        })
    }
    
    func Get_MeinfoTable(_ parameters:NSDictionary, view:UIView,block: @escaping(_ List:NSArray)-> Void){
        let afManager = AFHTTPRequestOperationManager()
        afManager.post("http://172.24.10.175/workout/api.php/getmine", parameters: parameters, success: {(operation:AFHTTPRequestOperation?, responseObject:Any?)  in
            print(responseObject)
            
        }, failure: {(operation:AFHTTPRequestOperation?, error:Error?) in
            MBProgressHUD.hide(for: view,animated: true)
            //MBProgressHUD.showDelayHUDToview(view, message: "网络加载失败")
        })
    }
    func searchhome(_ parameters:NSDictionary, view:UIView,block: @escaping(_ List:NSArray)-> Void){
        let afManager = AFHTTPRequestOperationManager()
        afManager.post("http://172.24.10.175/workout/api.php/hsearch/", parameters: parameters, success: {(operation:AFHTTPRequestOperation?, responseObject:Any?)  in
            print(responseObject)
            
        }, failure: {(operation:AFHTTPRequestOperation?, error:Error?) in
            MBProgressHUD.hide(for: view,animated: true)
            //MBProgressHUD.showDelayHUDToview(view, message: "网络加载失败")
        })
    }
    
    func Network_Update(_ parameters:NSDictionary, view:UIView, block: @escaping(_ flag:NSString)-> Void){
        post(urlStr:"http://172.24.10.175/workout/api.php/update", parameter: parameters){(res) in
            let data = res as! String
            if data > "0"{
                block("1")
            }else{
                block("0")
            }}}
    func Network_Create(_ parameters:NSDictionary, view:UIView, block: @escaping(_ flag:NSString)-> Void){
        post(urlStr:"http://172.24.10.175/workout/api.php/create", parameter: parameters){(res) in
            let data = res as! String
            if data > "0"{
                block("1")
            }else{
                block("0")
            }}}
    
    
    
    func fetchImage(_ block: @escaping(_ List:NSArray)-> Void){
        let afManager = AFHTTPRequestOperationManager()
        afManager.post("http://172.24.10.175/workout/api.php/lists/mod/itempos/SessionID/2ul9sj8vlfofj122e1s98048k2/related/1/posid/1", parameters: nil, success:
            { (datatask, json) in
                block(json as! NSArray)
        }, failure: {(operation:AFHTTPRequestOperation?, error:Error?) in
            //MBProgressHUD.hide(for: view,animated: true)
            //MBProgressHUD.showDelayHUDToview(view, message: "网络加载失败")
        })
    }
//    func fetchImage(block:@escaping (NSArray)->Void){
//        sessionManager.post(RO_Picture, parameters: nil, success: { (datatask, json) in
//            block(json as! NSArray)
//        }, failure: nil)
//    }
    
    private func post(urlStr:String,parameter:NSDictionary,block:((_ data:Any?)->Void)?){
        let paraArray = NSMutableArray()
        for (key,value) in parameter{
            let str = "\(key)=\(value)"
            paraArray.add(str)
        }
        let body = paraArray.componentsJoined(by: "&")
        var request = URLRequest(url: URL(string: urlStr)!)
        request.httpMethod = "post"
        request.httpBody = body.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if data != nil{
                if let json = try? JSONSerialization.jsonObject(with: data!, options: .mutableContainers){
                    block!(json)
                }
                else{
                    if let str = String(data: data!, encoding: String.Encoding(rawValue: CFStringConvertEncodingToNSStringEncoding(CFStringEncoding(CFStringEncodings.GB_18030_2000.rawValue)))){
                        print(str)
                    }else if let str = String(data: data!, encoding: .utf8){
                        print(str)
                    }
                }
            }else{
                print("返回为空")
                print(error.debugDescription)
            }
        }
        task.resume()
    }
    
}



