//
//  TrainDetailModel.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/1.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class ActiveDetailModel: NSObject {
    var id:NSString!
    var activity_name:NSString!
    var introduction:NSString!
    var start_date:NSString!
    var end_date:NSString!
    var thumb:NSString!
    var thumbsize:NSString!
    var activity_price:NSString!
    var activity_price_stu:NSString!
    var is_top:NSString!
    var attend_num:NSString!
    var sign_num:NSString!
    var view_num:NSString!
    var province_id:NSString!
    var province_name:NSString!
    var city_id:NSString!
    var city_name:NSString!
    var district_id:NSString!
    var district_name:NSString!
    var place:NSString!
    var user_id:NSString!
    var user_name:NSString!
    var contact_tel:NSString!
    var actcate_id:NSString!
    var actcate_name:NSString!
    var act_state:NSString!
    var club_id:NSString!
    var club_name:NSString!
    var notice:NSString!
    var userid:NSString!
    var hits:NSString!
    var insert_time:NSString!
    var update_time:NSString!
    var check_userid:NSString!
    var checktime:NSString!
    var checkstate:NSString!
    var listorder:NSString!
    var vstate:NSString!
    
    
    class func ActiveDetailModelWith(_ Dict:NSDictionary) -> ActiveDetailModel{
        let model = ActiveDetailModel()
        model.setValuesForKeys(Dict as! [String:NSObject])
        return model
        
    }
    
}


