//
//  ActivityCell.swift
//  LuYao
//
//  Created by Bobobo on 2017/10/24.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

protocol activityListDelegate {
    
}
class activityListCell:UITableViewCell{
    var activityDelegate:activityListDelegate!
    var AcitivityPhoto:UIImageView!
    var Activity_name:UILabel!
    var Introduction:UILabel!
    var indexPath:IndexPath!
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setView()
    }
    
    func setView(){
        AcitivityPhoto = UIImageView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 200))
        self.addSubview(AcitivityPhoto)
        
        Activity_name = UILabel(frame: CGRect(x: 125, y: 5, width: 160, height: 40))
        Activity_name.font = UIFont.systemFont(ofSize: 20)
        self.addSubview(Activity_name)
        
        Introduction = UILabel(frame: CGRect(x: 10, y: 45, width: 375, height: 40))
        Introduction.font = UIFont.systemFont(ofSize: 20)
        self.addSubview(Introduction)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        textLabel?.frame = CGRect(x: 10, y: 5, width: 120, height: 40)
        textLabel?.font = UIFont.systemFont(ofSize: 20)
    }
    
}


