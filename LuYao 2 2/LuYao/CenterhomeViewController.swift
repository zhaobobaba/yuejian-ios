//
//  CenterhomeViewController.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/1.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class CenterhomeViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    lazy var refresh = UIRefreshControl()
    
    @IBOutlet weak var segmentt: UISegmentedControl!
    var turebarbtnItem:UIBarButtonItem!
    var rightBtn:UIButton = UIButton.init()
    var teacherList = NSArray()
    var trainList = NSArray()
    @IBOutlet weak var centercollec: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        centercollec.delegate = self
        centercollec.dataSource = self
        getteacher()
        segmentt.selectedSegmentIndex = 1
        
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: width, height: height * 0.5)
        layout.sectionInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 5)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        centercollec.register(UINib.init(nibName: "TeacherTableCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        
        centercollec.register(UINib.init(nibName: "TrainCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "traincell")
        
        centercollec.collectionViewLayout = layout
        
        
        // Do any additional setup after loading the view.
    }
    
    @objc func refreshData(){
        getteacher()
        refresh.endRefreshing()
    }
    
    @IBAction func selectt(_ sender: UISegmentedControl) {
        switch segmentt.selectedSegmentIndex {
        case 0:
            gettrain()
        case 1:
            getteacher()
            
        default:
            break
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var basecell = UICollectionViewCell()
        
        if segmentt.selectedSegmentIndex == 1{
            let cell:TeacherTableCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as!TeacherTableCollectionViewCell
            
            
            let model:teachermodel = teacherList[indexPath.row] as! teachermodel
            
            
            cell.teachername.text = model.realname! as String
            
            let imageStr = "http://172.24.10.175/workout/Uploads/" + (model.thumb! as String)
            let imageUrl = URL(string: imageStr)
            let data = try! Data(contentsOf: imageUrl!)
            let image = UIImage(data: data)
            
            cell.pic.image = image
            
            let address = "\(model.province_name!)" + "\(model.city_name!)"
            cell.address.text = address
            
            if model.club_name == nil{
                cell.clubname.text = "#个人#"
            }else{
                
                cell.clubname.text = model.club_name as String
                
            }
            
            cell.text.text = model.introduction as String
            
            let strtec = "擅长:" + "\(model.actcate_name!)"
            
            cell.teachertec.text = strtec
            
            
            basecell = cell
            
        }
            
        else if segmentt.selectedSegmentIndex == 0{
            
            
            let cell:TrainCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "traincell", for: indexPath) as! TrainCollectionViewCell
            
            
            
            cell.clubbusername.text = ROLogonReturmModel.username
            cell.courseusername.text = ROLogonReturmModel.username
            //cell.coursename.text = trainList.
            
            cell.add.addTarget(self, action: #selector(totrain), for: .touchUpInside)
            
            basecell = cell
            
            
        }
        
        return basecell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if segmentt.selectedSegmentIndex == 1{
            if teacherList.count != 0{
                return teacherList.count
            }
        }
        else if segmentt.selectedSegmentIndex == 0{
            return 1
        }
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if segmentt.selectedSegmentIndex == 1{
            let model:teachermodel = teacherList[indexPath.row] as! teachermodel
            self.performSegue(withIdentifier: "toteacherdetail", sender: model.id)
        }
        //        else if segmentt.selectedSegmentIndex == 0{
        //            let model:ActiveModel = trainList[indexPath.row] as! ActiveModel
        //            let Dict = ["mod":model.rec_mod,"id":model.id,"name":model.rec_name] as NSDictionary
        //            self.performSegue(withIdentifier: "ToactiveDetail", sender: Dict)
        //
        //        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toteacherdetail"{
            let TeacherDetailController = segue.destination as! TeacherDetailTableViewController
            TeacherDetailController.teacherID = sender as! NSString
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func totrain(){
        self.performSegue(withIdentifier: "toselectcourse", sender: nil)
        
    }
    
    
    func getteacher(){
        let parameters = ["mod":"master","SessionID":ROLogonReturmModel.sessionID,"page":1] as NSDictionary
        RONetworkMngTool.sharedNetwotkmngTool().Network_GetteacherListView(parameters, view: self.view){(teacherList) in
            self.teacherList = teacherList
            self.centercollec.reloadData()
        }
    }
    func gettrain(){
        
        let parameters = ["mod":"acmrecom","SessionID":ROLogonReturmModel.sessionID,"page":1] as NSDictionary
        RONetworkMngTool.sharedNetwotkmngTool().Network_GetactiveListView(parameters, view: self.view){(trainList) in
            self.trainList = trainList
            //print(trainList)
            self.centercollec.reloadData()
        }
        
    }
    
    
    //最小item间距
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        return 0
    }
    
    //最小行间距
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if segmentt.selectedSegmentIndex == 0{
            return 0
        }
        return 0
    }
    //items尺寸
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if segmentt.selectedSegmentIndex == 1{
            return CGSize(width: width, height: height * 0.5)
        }
        else if segmentt.selectedSegmentIndex == 0{
            return CGSize(width:width, height: height)
        }
        return CGSize(width: 0, height: 0)
    }
    //偏移量
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    
    
}

