//
//  XibButtonTableViewCell.swift
//  LuYao
//
//  Created by 路瑶 on 2017/11/1.
//  Copyright © 2017年 luyao. All rights reserved.
//

import UIKit

class XibButtonTableViewCell: UITableViewCell {
    @IBOutlet weak var all: UIButton!
    
    @IBOutlet weak var jt: UIButton!
    @IBOutlet weak var cn: UIButton!
    @IBOutlet weak var rm: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    class func creatCell(_ tableView:UITableView)->XibButtonTableViewCell{
        var cell = tableView.dequeueReusableCell(withIdentifier: "XibButtonTableViewCell")
        if cell == nil{
            cell = (Bundle.main.loadNibNamed("XibButtonTableViewCell", owner: nil, options: nil))?.first as! XibButtonTableViewCell
        }
        return cell as! XibButtonTableViewCell
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

