//
//  ContestDetailViewController.swift
//  LuYao
//
//  Created by Bobobo on 2017/10/24.
//  Copyright © 2017年 luyao. All rights reserved.
//



import UIKit

class ContestDetailViewController: UIViewController {
    var infoDict:NSDictionary!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewConfig()
    }
    
    func viewConfig(){
        view.backgroundColor = UIColor.white
        
        //        let navBar = HomeStoryboard(frame: CGRect(origin: .zero, size: CGSize(width: WIDTH, height: 64)))
        //        navBar.initialize(text: nil)
        //        view.addSubview(navBar)
        
        let scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height-64))
        //scrollView.contentInset = .never
        scrollView.contentSize = CGSize(width: 0, height: 364+(UIScreen.main.bounds.height-69)/3)
        scrollView.backgroundColor = view.backgroundColor
        scrollView.showsVerticalScrollIndicator = false
        view.addSubview(scrollView)
        
        let imageView = UIImageView()
        let pURL = "http://172.24.10.175/workout/Uploads/"
        imageView.setImageWith(URL(string: pURL+(infoDict.value(forKey: "thumb") as! String)),
                               placeholderImage: UIImage(named:"placeholder"))
        imageView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: (UIScreen.main.bounds.height-69)/3)
        scrollView.addSubview(imageView)
        
        let label = UILabel(frame: CGRect(x: 15, y: imageView.bounds.height+10, width: UIScreen.main.bounds.width-30, height: 40))
        label.text = "简介"
        label.textAlignment = .center
        label.layer.cornerRadius = 5
        label.layer.borderColor = UIColor.lightGray.cgColor
        //label.layer.borderWidth = 1
        scrollView.addSubview(label)
        
        let textView = UITextView(frame: CGRect(x: 15, y: label.frame.origin.y+50, width: UIScreen.main.bounds.width-30, height: 100))
        textView.text = infoDict.value(forKey: "introduction") as! String
        textView.textAlignment = .center
        textView.isEditable = false
        textView.font = UIFont.systemFont(ofSize: 16)
        //textView.layer.borderWidth = 1
        textView.layer.borderColor = UIColor.lightGray.cgColor
        scrollView.addSubview(textView)
        
        let telLabel = UILabel(frame: CGRect(x: 15, y: textView.frame.origin.y+120, width: UIScreen.main.bounds.width-30, height: 20))
        telLabel.text = "比赛名称: \(infoDict.value(forKey: "name") as! String)"
        telLabel.font = UIFont.systemFont(ofSize: 20)
        scrollView.addSubview(telLabel)
        
        //        let addrLabel = UILabel(frame: CGRect(x: 15, y: telLabel.frame.origin.y+40, width: UIScreen.main.bounds.width-30, height: 20))
        //        let addr = (infoDict.value(forKey: "province_name") as! String)+(infoDict.value(forKey: "city_name") as! String)+(infoDict.value(forKey: "district_name") as! String)+(infoDict.value(forKey: "place" ) as! String)
        //        addrLabel.text = "具体地址: \(addr)"
        //        addrLabel.font = UIFont.systemFont(ofSize: 20)
        //        scrollView.addSubview(addrLabel)
        
        let startdateLable = UILabel(frame: CGRect(x: 15, y:telLabel.frame.origin.y+40, width: UIScreen.main.bounds.width-30, height: 20))
        //        let date = (infoDict.value(forKey: "start_date") as! String)+(infoDict.value(forKey: "end_date") as! String)
        startdateLable.text = "发布时间: \(infoDict.value(forKey: "update_time") as! String)"
        startdateLable.font = UIFont.systemFont(ofSize:20)
        scrollView.addSubview(startdateLable)
        
        let enddateLable = UILabel(frame: CGRect(x: 15, y: startdateLable.frame.origin.y+40, width: UIScreen.main.bounds.width-30, height: 20))
        //        let date = (infoDict.value(forKey: "start_date") as! String)+(infoDict.value(forKey: "end_date") as! String)
        enddateLable.text = "点击次数: \(infoDict.value(forKey: "hits") as! String)"
        enddateLable.font = UIFont.systemFont(ofSize:20)
        scrollView.addSubview(enddateLable)
        
    }
    
    
    
    @objc func back(){
        _ = navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}


